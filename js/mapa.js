
    var centro = ol.proj.transform([-58.417131, -34.558466], 'EPSG:4326', 'EPSG:3857');

    var raster = new ol.layer.Tile({
        source: new ol.source.OSM()        
      });

      //var wkt = 'LINESTRING(-68.418182 -31.571297,-68.252855 -31.338315,-68.24716666 -31.19816667,-68.22064455 -31.2010382,-68.19233185 -31.20818258,-68.16329167 -31.22087,-68.14526203 -31.23258054,-68.12905308 -31.24636729,-68.11407166 -31.26383667,-68.10213405 -31.28271645,-68.09329348 -31.30507163,-68.08921333 -31.33115333,-67.50133334 -31.3225)';

      // var anguloR13=-40;
      // var DistanciaR13=-1;
      // var R13x1=-58.42524;
      // var R13y1=-34.553942;
      // var R13x2=(Math.cos(anguloR13)*DistanciaR13)+R13x1;
      // var R13y2=(Math.sin(anguloR13)*DistanciaR13)+R13y1;
      
      // var anguloEZE19=-334;
      // var DistanciaEZE19=1;      
      // var EZE19x1=-58.564722216667;
      // var EZE19y1=-34.123333333333;
      // var EZE19x2=(Math.cos(anguloEZE19)*DistanciaEZE19)+EZE19x1;
      // var EZE19y2=(Math.sin(anguloEZE19)*DistanciaEZE19)+EZE19y1;


      // var wkt = 'Multilinestring(('+R13x1+' '+R13y1+','+R13x2+' '+R13y2+'),('+EZE19x1+' '+EZE19y1+','+EZE19x2+' '+EZE19y2+'))';


      var x1=-58.42524;
      var y1=-34.553942;

      var angulo=99;
      angulo = (-angulo + 90) * (Math.PI/180);
      var distancia = 20;

      var x2=(Math.cos(angulo)*distancia)+x1;
      var y2=(Math.sin(angulo)*distancia)+y1;
            
// wkt = 'MULTILINESTRING(('+x1+' '+y1+','+x1+' '+y1+'),';
  wkt = 'LINESTRING('+x1+' '+y1+','+x2+' '+y2+')';

// var px=((x1*y2-y1*x2)*(x3-x4)-(x1-x2)(x3*y4-y3*x4))/((x1-x2)(y3-y4)-(y1-y2)(x3-x4) );
// var py=((x1*y2-y1*x2)(y3-y4)-(y1-y2)(x3*y4-y3*x4))/((x1-x2)(y3-y4) - (y1-y2)(x3-x4) );

      var x3=-58.246666666667;
      var y3=-34.624444433333;
      var angulo=129;

      angulo = (-angulo + 90) * (Math.PI/180);
      // var distancia = 10;

      var x4=(Math.cos(angulo)*distancia)+x1;
      var y4=(Math.sin(angulo)*distancia)+y1;

      wkt = "MULTILINESTRING(("+x1+" "+y1+","+x2+" "+y2+"),("+x3+" "+y3+","+x4+" "+y4+"))";

      var A=y2-y1;  
      var A1=y4-y3;  
      var B=x1-x2;  
      var B1=x3-x4;  
      var C=(y1*x2)-(y2*x1);   
      var C1=(y3*x4)-(y4*x3);

      var px= ((B*C1)-(B1*C))/((A*B1)-(A1*B));
      var py= ((C*A1)-(C1*A))/((A*B1)-(A1*B));

      // alert(px+' '+py+' / '+(px*10000000000)/10000000000+' '+(py*10000000000)/10000000000);

      px=(px*10000000000)/10000000000;
      py=(py*10000000000)/10000000000;      



      var valido=false;
      A=(px-x1)/(x2-x1);
      A1=(py-y1)/(y2-y1);
      B=(px-x3)/(x4-x3);
      B1=(py-y3)/(y4-y3);

      if (((0<= A && A <= 1) || (0<=A1 && A1<=1)) && ((0<=B && B<=1) || (0<=B1 && B1<=1))) {
              valido=true;
      }
      
      // if (valido){
      //   alert('PUNTO VALIDO');
      // }else
      // {
      //   alert('PUNTO NO INTERSECA LAS RECTAS');
      // }


      var ruta='LINESTRING empty';     

      // var wkt_points = 'MULTIPOINT(('+px+' '+py+'),(-84.439025 33.649533),(-84.438361 33.646786),(-84.447967 33.634706),(-84.447967 33.631814),(-84.447878 33.620272),(-84.405508 33.646794),(-84.409453 33.649542),(-84.418400 33.631822),(-84.408908 33.634703),(-84.418317 33.620286),(-85.268172216667 34.281497216667),(-84.7933 33.634172216667),(-83.437202783333 33.167208333333),(-82.783030566667 36.249747216667),(-84.305675 33.649513883333),(-83.257327783333 35.436713883333),(-82.508088883333 31.536555566667),(-84.86295 33.630936116667),(-84.048658333333 33.915197216667),(-84.719441666667 33.634366666667),(-84.435063883333 33.629077783333),(-81.891047216667 35.817594433333),(-85.250197216667 32.785844433333),(-84.205588883333 33.649402783333),(-84.421383333333 32.961247216667),(-82.337422216667 31.747311116667),(-84.641286116667 33.649280566667),(-85.5751 34.790997216667),(-83.7435 33.185513883333),(-84.759366666667 33.699880566667),(-82.689583333333 35.186663883333),(-84.179702783333 33.632833333333),(-84.831961116667 31.918355566667),(-83.093425 33.306083333333),(-82.348561116667 34.445091666667),(-86.00875 32.575497216667),(-86.263636116667 32.428661116667),(-83.955627783333 33.720775),(-86.684772216667 36.136961116667),(-83.859416666667 35.799330566667),(-84.515802783333 33.733027783333),(-84.132597216667 33.646533333333),(-84.331144433333 33.732938883333),(-84.856188883333 33.843758333333),(-83.024963883333 34.637805566667),(-84.549405566667 33.631727783333),(-86.443433333333 36.928741666667),(-88.294472216667 32.438177783333),(-86.019366666667 34.345252783333),(-83.966172216667 33.922905566667),(-84.545161116667 33.631744433333),(-83.807725 33.216958333333),(-81.801802783333 30.839308333333),(-87.455947216667 33.934616666667),(-84.154477783333 33.337383333333),(-84.968775 34.143830566667),(-83.744133333333 33.200180566667),(-85.048219433333 32.586575),(-86.355316666667 32.601413883333),(-84.525613883333 33.814819433333),(-84.286502783333 34.771583333333),(-83.816925 33.654533333333),(-88.270975 31.855105566667),(-84.512836116667 33.634677783333),(-81.509927783333 30.338880566667),(-84.651052783333 33.620016666667),(-83.048791666667 29.599),(-84.207486116667 33.938577783333),(-85.201783333333 33.818652783333),(-84.716166666667 33.536566666667),(-83.945611116667 33.293566666667),(-84.848022216667 33.95225),(-82.830044433333 32.563461116667),(-84.082013883333 33.567025),(-84.308202783333 33.646769433333),(-85.732813883333 34.771761116667),(-84.308277783333 33.631791666667),(-87.435166666667 34.618080566667),(-84.790711116667 33.619647216667),(-84.793886116667 33.631177783333),(-82.766311116667 34.783880566667),(-83.902280566667 33.891966666667),(-83.295080566667 34.484113883333),(-86.368183333333 35.746927783333),(-84.892447216667 33.692888883333),(-86.183083333333 33.06475),(-83.567102783333 32.947166666667),(-84.584583333333 33.536791666667),(-84.649769433333 33.631561116667),(-84.641563883333 33.732766666667),(-81.864819433333 33.265075),(-84.650747216667 33.536530566667),(-85.472255566667 35.597725),(-86.042772216667 33.687538883333),(-86.317297216667 31.689280566667),(-84.838044433333 33.319191666667),(-85.021927783333 34.087208333333),(-84.100555566667 33.634166666667),(-80.316283333333 37.017755566667),(-83.440477783333 33.238244433333),(-86.091330566667 32.701038883333),(-84.307327783333 33.536891666667),(-83.566969433333 32.947038883333),(-86.447205566667 32.321758333333),(-84.276361116667 33.536933333333),(-84.584638883333 33.588155566667),(-84.708263883333 33.619888883333),(-84.279736116667 33.732977783333),(-83.638388883333 33.509922216667),(-83.321044433333 34.557647216667),(-88.486127783333 31.097961116667),(-84.336133333333 33.455827783333),(-84.718836116667 33.631397216667),(-85.113236116667 32.503755566667),(-85.051283333333 34.049916666667),(-84.780358333333 36.422438883333),(-85.070930566667 33.11755),(-85.372988883333 31.853366666667),(-85.765855566667 32.367216666667),(-84.805872216667 31.867075),(-84.857611116667 33.645891666667),(-84.231797216667 33.942508333333),(-82.15415 34.251566666667),(-86.567772216667 33.077077783333),(-84.028741666667 33.363022216667),(-84.276336116667 33.602144433333),(-84.2142 33.631683333333),(-84.587763883333 32.986613883333),(-83.968188883333 33.970961116667),(-84.130341666667 33.649266666667),(-85.004755566667 33.15825),(-85.648530566667 33.838355566667),(-84.546394433333 33.646691666667),(-84.791461116667 33.956313883333),(-83.778069433333 35.446269433333),(-84.134777783333 33.537025),(-82.320247216667 37.075),(-85.020361116667 33.179288883333),(-84.722788883333 33.732588883333),(-85.028602783333 33.183777783333),(-81.863019433333 33.057177783333),(-84.179177783333 33.848480566667),(-84.650177783333 33.634511116667),(-84.279566666667 33.682769433333),(-84.150086116667 34.763675),(-83.980208333333 33.330458333333),(-84.276388883333 33.456986116667),(-84.037238883333 33.73305),(-82.162063883333 33.707352783333),(-87.041319433333 31.978230566667),(-85.748661116667 35.546286116667),(-84.710147216667 33.649116666667),(-83.649877783333 33.760652783333),(-84.832677783333 33.899908333333),(-84.144441666667 33.620061116667),(-84.214975 33.536688883333),(-86.076661116667 34.44585),(-84.011413883333 33.646225),(-83.822711116667 33.190394433333),(-86.149994433333 34.336933333333),(-84.8299 33.299955566667),(-84.315180566667 33.620272216667),(-85.224475 33.549675),(-84.427863883333 33.6367),(-84.038875 33.733183333333),(-82.171366666667 34.525894433333),(-87.049677783333 32.335652783333),(-84.137788883333 33.732736116667),(-84.280005566667 33.812508333333),(-84.434602783333 33.732952783333),(-83.714511116667 34.633622216667),(-82.879325 36.921236116667),(-84.564677783333 33.732941666667),(-84.138797216667 33.733080566667),(-83.143408333333 33.748202783333),(-84.857686116667 33.648641666667),(-82.978102783333 32.379741666667),(-82.876516666667 33.306380566667),(-84.330194433333 33.634683333333),(-82.313155566667 35.017138883333),(-85.079816666667 35.114886116667),(-85.170033333333 34.360097216667),(-85.206222216667 33.049119433333),(-84.269202783333 33.633636116667),(-82.803116666667 33.124991666667),(-84.408138883333 32.300347216667),(-84.129047216667 33.634305566667),(-85.397886116667 34.140022216667),(-84.201630566667 33.646655566667),(-83.827538883333 33.58405),(-83.647183333333 32.691186116667),(-88.804266666667 32.378438883333),(-82.089505566667 33.969844433333),(-89.983208333333 35.015116666667),(-86.319727783333 32.222280566667),(-86.030716666667 35.269469433333),(-82.88265 34.937930566667),(-84.059983333333 33.634108333333),(-86.833333333333 32.597436116667),(-79.106888883333 37.900525),(-86.798866666667 32.752541666667),(-84.582030566667 33.693038883333),(-84.204102783333 33.634472216667),(-84.844497216667 34.012516666667),(-86.186575 35.430077783333),(-82.682886116667 33.985530566667),(-85.226113883333 33.698594433333),(-84.897291666667 33.739177783333),(-85.833925 35.078297216667),(-83.194216666667 32.589388883333),(-82.559447216667 34.664397216667),(-84.640411116667 33.646533333333),(-86.154022216667 32.866055566667),(-85.718330566667 32.755491666667),(-83.297661116667 34.695872216667),(-84.205444433333 33.732888883333),(-83.973558333333 34.194119433333),(-87.956930566667 32.539597216667),(-84.304480566667 33.634627783333),(-85.312244433333 34.238694433333),(-83.834486116667 34.095633333333),(-84.401197216667 34.296825),(-87.885530566667 31.467875),(-84.709477783333 33.646369433333),(-84.713311116667 34.255386116667),(-83.637172216667 33.656316666667),(-84.239219433333 33.633738883333),(-83.727005566667 33.952822216667),(-85.774858333333 33.279327783333),(-84.030802783333 33.461644433333),(-83.959111116667 33.754041666667),(-85.210291666667 33.419694433333),(-84.06245 33.619875),(-83.061155566667 35.775022216667),(-84.829897216667 33.452763883333),(-86.42725 32.8184),(-84.404961116667 35.047963883333),(-84.581580566667 33.732947216667),(-84.336183333333 33.631827783333),(-81.753344433333 30.756522216667),(-84.548777783333 33.536813883333),(-84.920086116667 35.591822216667),(-84.53877905 33.639128316667),(-84.560903066667 33.6510797),(-84.576044666667 33.669058016667),(-84.697458333333 33.576755566667),(-84.223605566667 33.384780566667),(-83.338755566667 32.905625),(-84.667952783333 33.420075),(-85.119397216667 34.162561116667),(-82.615175 34.871619433333),(-84.525 33.536866666667),(-84.340091666667 33.536947216667),(-85.325186116667 32.226952783333),(-84.070083333333 33.631383333333),(-84.179091666667 33.684866666667),(-84.532669433333 33.634675),(-83.647413883333 34.307844433333),(-86.937413883333 34.543630566667),(-86.706880566667 34.483780566667),(-84.863163883333 33.633947216667),(-84.572161116667 32.298147216667),(-84.435069433333 33.378638883333),(-84.554094433333 33.649430566667),(-81.852952783333 35.792497216667),(-84.138947216667 33.631547216667),(-84.871633333333 33.240513883333),(-84.430480566667 33.536841666667),(-82.963511116667 34.125938883333),(-87.790205566667 31.525952783333),(-83.604866666667 33.081188883333),(-83.822541666667 33.190511116667),(-84.343116666667 33.633647216667),(-81.870177783333 33.608777783333),(-84.657605566667 33.634536116667),(-84.001219433333 33.631180566667),(-85.221955566667 33.023758333333),(-84.095930566667 33.999994433333),(-84.261794433333 34.288288883333),(-84.172561116667 32.998561116667),(-84.340011116667 33.813052783333),(-84.005483333333 35.911547216667),(-84.816086116667 33.732313883333),(-84.583222216667 33.67005),(-83.052369433333 35.7901),(-84.749644433333 33.545830566667),(-81.927011116667 35.033625),(-87.862902783333 32.733797216667),(-84.9187 33.922266666667),(-87.453527783333 33.117633333333),(-84.976752783333 32.220588883333),(-83.887988883333 34.329263883333),(-84.816086116667 33.536427783333),(-84.305313883333 33.732938883333),(-84.373944433333 30.556227783333),(-82.290405566667 35.425508333333),(-83.099255566667 34.750672216667),(-85.038636116667 33.611302783333),(-83.144466666667 34.308630566667),(-83.952594433333 33.47635),(-82.98025 36.278341666667),(-85.544747216667 32.800380566667),(-84.866030566667 33.306452783333),(-84.0136 33.336805566667),(-84.716166666667 33.732591666667),(-84.261183333333 33.390480566667),(-84.435072216667 33.879527783333),(-84.257958333333 33.671288883333),(-82.12 32.403733333333),(-84.512711116667 33.456794433333),(-84.594230566667 34.283636116667),(-84.010105566667 33.958575),(-84.549841666667 33.63465),(-84.078530566667 33.917719433333),(-81.890897216667 30.212994433333),(-83.974769433333 33.084180566667),(-86.899836116667 33.670133333333),(-83.913083333333 32.365508333333),(-83.463452783333 35.071216666667),(-84.729266666667 33.634847216667),(-83.848055566667 34.083166666667),(-84.142280566667 33.559269433333),(-83.627513883333 34.290316666667),(-82.618163883333 35.161011116667),(-84.584625 33.573975),(-83.770041666667 33.140536116667),(-84.214741666667 33.620175),(-84.581066666667 33.846619433333),(-84.625111116667 31.3229),(-83.437405566667 33.167222216667),(-87.480897216667 33.793416666667),(-84.909927783333 33.847794433333),(-85.663686116667 32.712444433333),(-81.197025 36.322080566667),(-84.911291666667 33.493408333333),(-83.991116666667 33.633875),(-84.035441666667 33.537130566667),(-84.584494433333 33.457091666667),(-84.580694433333 33.811277783333),(-84.011352783333 33.648958333333),(-84.550980566667 33.620169433333),(-83.827147216667 34.996505566667)) ';

      var geojsonObject = { 'type': 'FeatureCollection',
                  'features': [
                  { 'type': 'Feature',
                  'geometry': {'type': 'Point', 'coordinates': [px , py ]},
                  'properties': {'fixname': 'PXPY'}
                  },
                  { 'type': 'Feature',
                  'geometry': {'type': 'Point', 'coordinates': [-58.243055566667, -34.536666666667]},
                  'properties': {'fixname': 'AER14'}
                  }
                  ]}

      var format = new ol.format.WKT();
      var format_GeoJSON = new ol.format.GeoJSON();

 // var features_fijos  = new ol.format.GeoJSON().readFeatures(
 //                 geojsonObject,                
 //                {
 //                    dataProjection: 'EPSG:4326',
 //                    featureProjection: 'EPSG:3857'
 //                }                        
 //          );
      
 //      var vectorSourceFIXs = new ol.source.Vector(
 //        {
 //          features: features_fijos
 //         // url: 'map.geojson'
 //         // features: (new ol.format.GeoJSON()).readFeatures(geojsonObject,
 //         //          {
 //         //            dataProjection: 'EPSG:4326',
 //         //            featureProjection: 'EPSG:3857'}
 //         //          ),
        
 //      });
      //-------------------------------------------
      //   POLYGONO   LINESTRING
      //-------------------------------------------    
      var Ruta_style = function(feature, resolution){
            var estilo= new ol.style.Style({
                    // estilo del punto FIX:
                    stroke: new ol.style.Stroke({
                                      color: '#FF0000',
                                      width: 6
                                    })
                    });
                                
            return estilo;
      }

      var Ruta_style_approach = function(feature, resolution){
            var estilo= new ol.style.Style({
                    // estilo del punto FIX:
                    stroke: new ol.style.Stroke({
                                      color: '#0000FF',
                                      width: 6
                                    })
                    });
                                
                  return estilo;
          }      

      var Final_Approach_style = function(feature, resolution){
           var estilo = new ol.style.Style({
            stroke: new ol.style.Stroke({
                      width: 4, 
                      color: 'rgba(0, 0, 0, 1)',
                      lineDash: [3, 6] //or other combinations 
                      // [a,b]= a:largo del punto, b:separacion
            }),
            zIndex: 2
          });
          return estilo;
        }

      var feature_linestring = format.readFeature(ruta, {
        dataProjection: 'EPSG:4326',
        featureProjection: 'EPSG:3857'
      });

      // ------------------------------------------------------
      //   RUTA DE SID o STAR
      // ------------------------------------------------------
      var source_linestring=new ol.source.Vector(
                  {
                        features: [feature_linestring]                          
                  });

      var vector = new ol.layer.Vector({
                  source: source_linestring
                  ,style: Ruta_style
                 });


      // ------------------------------------------------------
      // RUTA DE APPROACHES
      // ------------------------------------------------------
      var approach_linestring=new ol.source.Vector(
                  {
                        features: [feature_linestring]
                  });
      var vector_approaches = new ol.layer.Vector({
                  source: approach_linestring
                  ,style: Ruta_style_approach
                  });    

      // ------------------------------------------------------
      // RUTA DE FINAL APPROACH STAR (tramo a pista)
      // ------------------------------------------------------
      var final_app_linestring=new ol.source.Vector(
                  {
                        features: [feature_linestring]
                  });
      var vector_final_app = new ol.layer.Vector({
                  source: final_app_linestring
                  ,style: Final_Approach_style    // estilo LINEA DE PUNTOS
                  });      


      //-------------------------------------------
      // VECTOR PUNTOS
      //-------------------------------------------

      // var getText = function(feature) {
      //     var text = feature.get('name');
      //     return text;
      // };

      const getText = function(feature) {
        const type = 'wrap';
        const maxResolution = 1.200;
        let text = feature.get('name');

        // if (resolution > maxResolution) {
        //   text = '';
        // } else if (type == 'hide') {
        //   text = '';
        // } else if (type == 'shorten') {
        //   text = text.trunc(12);
        // } else if (type == 'wrap'){ // && (!dom.placement || dom.placement.value != 'line')) {
        //   text = stringDivider(text, 16, '\n');
        // }

        return text;
      };      

      var createTextStyle = function(feature) {
        return new ol.style.Text({
          textAlign: 'center',
          textBaseline: 'middle',
          font: '12px Verdana',
          text: getText(feature),
          fill: new ol.style.Fill({color: 'black'}),
          stroke: new ol.style.Stroke({color: 'white', width: 0.5})
        });
      };      

      // // fuente del FEATURE = WellKnowText (WKT)
      // var feature_points = format.readFeature(wkt_points, {
      
      // // Fuente del FEATURE = GeoJSON
      // // var feature_points = format_GeoJSON.readFeatures(geojsonObject, {
      //   dataProjection: 'EPSG:4326',
      //   featureProjection: 'EPSG:3857'
      // });      

      // var vector_points = new ol.layer.Vector({
      //   source: new ol.source.Vector({
      //     features: [feature_points]
      //   }),
      //   // // style: styleFunction
      //   // style: new ol.style.Style({
      //   //           image: new ol.style.Circle({
      //   //               fill: new ol.style.Fill({ color: [255,0,0,1] }),
      //   //               stroke: new ol.style.Stroke({ color: [0,0,0,1] }),
      //   //               radius: 5
      //   //           }),         
      //   //           text: createTextStyle(feature_points)
      //   //         })
      // });  

      // // var vector_points = new ol.layer.Vector({
      // //         layer:'fijos',
      // //         source: new ol.source.GeoJSON ({
      // //                 projection: 'EPSG:3857',
      // //                 url: 'migeojson.geojson.php'
      // //         })
      // // });   


      
      var features_fijos  = new ol.format.GeoJSON().readFeatures(
                 geojsonObject,                
                {
                    dataProjection: 'EPSG:4326',
                    featureProjection: 'EPSG:3857'
                }                        
          );
      
      var vectorSourceFIXs = new ol.source.Vector(
        {
          features: features_fijos
         // url: 'map.geojson'
         // features: (new ol.format.GeoJSON()).readFeatures(geojsonObject,
         //          {
         //            dataProjection: 'EPSG:4326',
         //            featureProjection: 'EPSG:3857'}
         //          ),
        
      });

//// ACTIVAR ESTO SI SE QUIERE CLUSTER
// var clusterSource = new ol.source.Cluster({
//   distance: 15,
//   source: vectorSourceFIXs
// });

var Init_style = function(feature, resolution){
      var estilo= new ol.style.Style({
              // estilo del punto FIX:
              image: new ol.style.Circle({
                  // fill: new ol.style.Fill({ color: [255,0,0,0.8] }),
                  fill:  new ol.style.Fill({ color: [200,0,0,0] }),
                  stroke: new ol.style.Stroke({ color: [0,0,0,0] }),
                  radius: 5
              })
              // estilo del LABEL de los FIJOS             

      });           
      return estilo;
}
var STAR_style = function(feature, resolution){
      var estilo= new ol.style.Style({
              // estilo del punto FIX:
              image: new ol.style.Circle({
                  // fill: new ol.style.Fill({ color: [255,0,0,0.8] }),
                  fill:  new ol.style.Fill({ color: [200,0,0,0.8] }),
                  stroke: new ol.style.Stroke({ color: [0,0,0,1] }),
                  radius: 5
              }),         
              // estilo del LABEL de los FIJOS
              text:  new ol.style.Text({
          
                    textAlign: 'left',
                    textBaseline: 'bottom',
                    font: '14px Verdana',                                
                    // font: '13px helvetica,sans-serif',
                    text: feature.get('fixname') ,
                    // rotation: 40 * (Math.PI / 180),
                    fill: new ol.style.Fill({
                        color: '#700909'
                       }),
                    stroke: new ol.style.Stroke({
                        color: '#fff',
                        width: 2
                    })
                })
              

      });           
      return estilo;
}


var SID_style = function(feature, resolution){
      var estilo= new ol.style.Style({
              // estilo del punto FIX:
              // image: new ol.style.Icon({
              //               anchor: [20, 20],
              //               size: [64, 64],
              //               offset: [64, 0],
              //               opacity: 1,
              //               scale: 1,
              //               src: 'https://openlayers.org/en/v3.20.1/examples/data/icon.png'
              //             }),              
              image: new ol.style.Circle({
                  // fill: new ol.style.Fill({ color: [255,0,0,0.8] }),
                  fill:  new ol.style.Fill({ color: [0,100,0,0.8] }),
                  stroke: new ol.style.Stroke({ color: [0,0,0,1] }),
                  radius: 5
              }),         
              // estilo del LABEL de los FIJOS
              text:  new ol.style.Text({          
                    textAlign: 'left',
                    textBaseline: 'bottom',
                    font: '14px Verdana',                                
                    // font: '13px helvetica,sans-serif',
                    text: feature.get('fixname') ,
                    // rotation: 40 * (Math.PI / 180),
                    fill: new ol.style.Fill({
                        color: '#0B2008'                        
                       }),
                    stroke: new ol.style.Stroke({
                        color: '#fff',
                        width: 2
                    })
                })
              
      });           
      return estilo;
}


      var vectorlayer_fixs = new ol.layer.Vector({
       source: vectorSourceFIXs, // // ACTIVAR ESTO SI NO SE QUIERE CLUSTER
       // source: clusterSource,  // ACTIVAR ESTO SI SE QUIERE CLUSTER

       //==========ESTILO PRUEBA=============
       style: Init_style
       //=========ESTILO OK FUNCIONANDO================
       // style: function(feature, resolution){
       //            var estilo= new ol.style.Style({
       //                    // estilo del punto FIX:
       //                    image: new ol.style.Circle({
       //                        // fill: new ol.style.Fill({ color: [255,0,0,0.8] }),
       //                        fill:  new ol.style.Fill({ color: [255,0,0,0.8] }),
       //                        stroke: new ol.style.Stroke({ color: [0,0,0,1] }),
       //                        radius: 7
       //                    }),         
       //                    // estilo del LABEL de los FIJOS
       //                    text:  new ol.style.Text({
                      
       //                          textAlign: 'left',
       //                          textBaseline: 'bottom',
       //                          font: '14px Verdana',                                
       //                          // font: '13px helvetica,sans-serif',
       //                          text: feature.get('fixname') ,
       //                          // rotation: 40 * (Math.PI / 180),
       //                          fill: new ol.style.Fill({
       //                              color: '#A00'
       //                             }),
       //                          stroke: new ol.style.Stroke({
       //                              color: '#fff',
       //                              width: 2
       //                          })
       //                      })
                          

       //            });           
       //        return estilo;
       //      }
       //=====================================================

        //// ESTE ESTILO FUNCIONA OK. Coloca el LABEL.....!!!!!!
        // style: function(feature, resolution) {
        //     var estilo1 = new ol.style.Style({
        //         stroke: new ol.style.Stroke({
        //             color: 'rgba(255,0,0,1.0)',
        //             width: 1
        //         }),
        //         fill: new ol.style.Fill({
        //             color: 'rgba(253,184,184,0.5)'
        //         }),
        //         text: new ol.style.Text({
        //             text: feature.get('fixname')
        //         })
        //     });
           
        //     return estilo1;
        // }
      });

     // // Create a heatmap layer based on GeoJSON content
     // // HABILITAR ESTA CAPA y AGREGARLA AL MAP para ver un HEATMAP de un VECTOR LAYER
     //  var heatmapLayer = new ol.layer.Heatmap({
     //            source: vectorSourceFIXs,
     //            opacity: 0.9
     //        });



      //-------------------------------------------
      // MAPA
      //-------------------------------------------

      var vista=new ol.View({
          center: centro, // -33.8088288395207, -59.0921780616523 ],
          zoom: 8
        });

      var map = new ol.Map({
        layers: [raster, vector, vectorlayer_fixs, vector_final_app, vector_approaches ],
        // layers: [raster, vector,heatmapLayer ],
        target: 'map',
        loadTilesWhileAnimating: true,
        view: vista
      });


function flyTo(location, done) {
  const duration = 10000;
  const zoom = vista.getZoom();
  let parts = 2;
  let called = false;
  function callback(complete) {
    --parts;
    if (called) {
      return;
    }
    if (parts === 0 || !complete) {
      called = true;
      done(complete);
    }
  }
  vista.animate({
    center: location,
    duration: duration
  }, callback);
  vista.animate({
    zoom: zoom - 1,
    duration: duration / 2
  }, {
    zoom: zoom,
    duration: duration / 2
  }, callback);
}


function UpdateMapGeoJSON(output,new_fixes){

         //======================================
        // borrar FIXES anteriores:
        // ORIGINAL: var vectorlayer_fixs = new ol.layer.Vector({
        var features = vectorlayer_fixs.getSource().getFeatures();
        features.forEach((feature) => {
            vectorlayer_fixs.getSource().removeFeature(feature);            
        });


        //======================================
        // Crea el nuevo objeto GeoJSON
        geojsonObject = new_fixes;
              // cargar FIXES:
        var features_fijos  = new ol.format.GeoJSON().readFeatures(
                          geojsonObject,      
                            {
                                dataProjection: 'EPSG:4326',
                                featureProjection: 'EPSG:3857'
                            }                        
                      );    

        // Coloca los nuevos FEATURES en el mapa
        vectorSourceFIXs.addFeatures( features_fijos );                                  
        //======================================
      
        //cambia color segun si es SID o STAR
            vectorSourceFIXs.forEachFeature(function(feat){
                      if (document.getElementById('selector_tipoprocedimiento').value=="SID") {
                        //so set the style on each feature
                              feat.setStyle(SID_style);
                      } else {
                      //and if doesnt exist switch back to the deafult style
                            feat.setStyle(STAR_style);
                      }
                  })

 
        // usar funcion "Number(lon) y Number(lat)" 
        // sino fallara la funcion TRANSFORM.
        // O sino en PHP multiplcar x 1 las respuestas
        // var lonlat = [output.centro[0].lon, output.centro[0].lat];
        var lonlat = [output.centro.lon, output.centro.lat];
        centro = ol.proj.transform(lonlat,'EPSG:4326', 'EPSG:3857');

        // ir al nuevo centro          

        // flyTo(centro,function() {});
        map.getView().setCenter(centro);
        
}                                      


//======================================
// Limpiar las rutas SIDS o STAR
//======================================
function clear_route(){
    //------------------------
    //   SID o STAR
    //------------------------
     var features = vector.getSource().getFeatures();
     features.forEach((feature) => {
          vector.getSource().removeFeature(feature);
     });

     //------------------------
     // FINAL APP
     //------------------------
     var features = vector_final_app.getSource().getFeatures();
     features.forEach((feature) => {
          vector_final_app.getSource().removeFeature(feature);
     });     
}


//======================================
// Limpiar las rutas APPROACHES
//======================================
function clear_approach(){
     var features = vector_approaches.getSource().getFeatures();
     features.forEach((feature) => {
          vector_approaches.getSource().removeFeature(feature);
     });
}


function UpdateMap_RUTA(new_ruta){

      //======================================
        // borrar FIXES anteriores:
        // var features = vector.getSource().getFeatures();
        // features.forEach((feature) => {
        //     vector.getSource().removeFeature(feature);            
        // });
       // ORIGINAL: var vectorlayer_fixs = new ol.layer.Vector({
       var features = vector.getSource().getFeatures();
       features.forEach((feature) => {
            vector.getSource().removeFeature(feature);
       });

       ruta = new_ruta;
       var feature_linestring = new ol.format.WKT().readFeatures(
                          ruta,      
                            {
                                dataProjection: 'EPSG:4326',
                                featureProjection: 'EPSG:3857'
                            }                        
                      );    

       source_linestring.addFeatures(feature_linestring);

        
}


function Update_RUTA_APPROACH(new_ruta){

      //======================================
        // borrar FIXES anteriores:
        // var features = vector.getSource().getFeatures();
        // features.forEach((feature) => {
        //     vector.getSource().removeFeature(feature);            
        // });
       // ORIGINAL: var vectorlayer_fixs = new ol.layer.Vector({
       var features = vector_approaches.getSource().getFeatures();
       features.forEach((feature) => {
            vector_approaches.getSource().removeFeature(feature);
       });

       ruta2 = new_ruta;
       var feature_linestring = new ol.format.WKT().readFeatures(
                          ruta2,      
                            {
                              dataProjection: 'EPSG:4326',
                              featureProjection: 'EPSG:3857'
                            }                        
                      );    

       approach_linestring.addFeatures(feature_linestring);
        
}

function Update_RUTA_FINAL_APP(new_ruta){

      //======================================
        // borrar FIXES anteriores:
        // var features = vector.getSource().getFeatures();
        // features.forEach((feature) => {
        //     vector.getSource().removeFeature(feature);            
        // });
       // ORIGINAL: var vectorlayer_fixs = new ol.layer.Vector({
       var features = vector_final_app.getSource().getFeatures();
       features.forEach((feature) => {
            vector_final_app.getSource().removeFeature(feature);
       });

       ruta2 = new_ruta;
       var feature_linestring = new ol.format.WKT().readFeatures(
                          ruta2,      
                            {
                                dataProjection: 'EPSG:4326',
                                featureProjection: 'EPSG:3857'
                            }                        
                      );    

       final_app_linestring.addFeatures(feature_linestring);
        
}

//--------------------------------------------------
// function que busca un FIX entre los que existen
//--------------------------------------------------
function buscar_fix(fix){
     var features = vectorlayer_fixs.getSource().getFeatures();
     var found=false;
     features.forEach((feature) => {
          if (feature.N.fixname==fix){
            var lonlat = [feature.N.geometry.A[0], feature.N.geometry.A[1]];
            // centro = ol.proj.transform(lonlat,'EPSG:4326', 'EPSG:3857');
            map.getView().setCenter(lonlat);
            map.getView().setZoom(13);
            found=true;
            $('#txt_fix').val("");
          }
     });
     if (!found){
        alert("No se encontró el FIX...");
     }
}

