<?php 

Class Controller_procedures extends CI_Controller{

	public function __construct(){
		parent::__construct();
		$this->load->model('Model_procedures');
	}	

	// ==========================================
	// INFORMACION DEL AEROPUERTO:
	// Respuesta: JSON con:
	//			- Array Assoc: LON LAT del centro de aeropuerto
	//			- Array Assoc: LISTA DE PISTAS (para el combo)
	//			- String: Nombre del Aeropuerto
	// ==========================================	
	function get_airportdata($xicao=false,$xrunway=false){

		if ($xicao!=false){
			$icao=$xicao;
		}else
		{
			$icao=$this->input->get('icao');
		}
		if ($xrunway!=false){
			$runway = $xrunway;
		}else{
			$runway=false;
		}

		$resu=$this->Model_procedures->get_airportdata($icao,$runway);

		echo json_encode($resu);

	}

	// ==========================================
	// INFO de los SID y STAR
	// Respuesta: JSON con:
	//			- Array Assoc: LISTA de SID o STAR (para el combo de procedimientos)
	// ==========================================	
	function get_procedures(){
		 $icao = $this->input->get('icao');
		 $runway= $this->input->get('runway');
		 $tipo_proc=$this->input->get('tipo_proc');

		 $resu=$this->Model_procedures->get_procedures($icao,$runway,$tipo_proc);

		 echo json_encode($resu);

	}


	// ==========================================
	// INFO de los SID y STAR
	// Respuesta: JSON con:
	//			- Array Assoc: LISTA de TRANSICIONES de SID o STAR
	//          - wkt: ROUTE con la linea a dibujar sin TRANSICIONES
	// ==========================================	
	function get_transitions(){
		 $icao = $this->input->get('icao');
		 $runway= $this->input->get('runway');
		 $tipo_proc=$this->input->get('tipo_proc');
         $proc_name=$this->input->get('proc_name');

		 $resu=$this->Model_procedures->get_procedures($icao,$runway,$tipo_proc,$proc_name,false);

		 echo json_encode($resu);

	}	


	// ==========================================
	// INFO de los SID y STAR
	// Respuesta: JSON con:
	//			- Array Assoc: LISTA de TRANSICIONES de SID o STAR
	//          - wkt: ROUTE con la linea a dibujar CON TRANSICIONES INCLUIDAS
	// ==========================================	
	function get_fullroute(){
		 $icao = $this->input->get('icao');
		 $runway= $this->input->get('runway');
		 $tipo_proc=$this->input->get('tipo_proc');
         $proc_name=$this->input->get('proc_name');
		 $transition_name=$this->input->get('transition');	

		 $resu=$this->Model_procedures->get_procedures($icao,$runway,$tipo_proc,$proc_name,$transition_name);

		 echo json_encode($resu);

	}	

	//============================================
	// 	APPROACHES
	//============================================
	function get_approach(){

		$icao=$this->input->get('icao');
		$approach=$this->input->get('approach');

		$resu = $this->Model_procedures->get_approach($icao,$approach,"");

		echo json_encode($resu);
	}

	//============================================
	// 	APPROACHES
	//============================================
	function get_approach_transitions(){

		$icao=$this->input->get('icao');
		$approach=$this->input->get('approach');
		$transicion=$this->input->get('transition_app');

		$resu = $this->Model_procedures->get_approach($icao,$approach,$transicion);

		echo json_encode($resu);
	}



	// function vectores_old(){

	// 	$data=$this->Model_procedures->get_airportdata("SABE");
	// 	$fixs=$data['fixs'];
	// 	// ///##################################################################################
	// 	// //  MEJORA PARA INCLUIR TRK y HDG INTERCEPTING RADIALS
	// 	// ///##################################################################################
	// 	// SID KUKEN7
	// 	$linea="RNW 13 TRK 132 UNTIL 418 HDG 040 INTERCEPT RADIAL 097 FROM FIX FDO HDG 359 INTERCEPT RADIAL 029 TO FIX KUKEN FIX KUKEN";
	// 	//  RNW 31 TRK 312 UNTIL 418 HDG 040 INTERCEPT RADIAL 029 TO FIX KUKEN FIX KUKEN

	// 	$alinea=explode(" ",$linea);

	// 	// echo ((2000*100)/111000)."<br>";  // 2000 metros = 1.80 grados mas o menos

	// 	$vector_number=0;
	// 	$vectors=array();
	// 	$last_pos="";
	// 	$correccion=-90; // factor de correccion para llevar 0° a al norte en lugar de estar hacia el ESTE.

	// 	for ($i=0; $i<count($alinea);$i++){
	// 	  if ($alinea[$i]=='RNW'){
	// 		$rnw=$alinea[$i+1];
	// 		$vectors["R.".$rnw]=trim($fixs["R.".$rnw]);	// origen: la pista
	// 		$last_pos=$vectors["R.".$rnw];  // primer coordenada
	// 	  } 

		  
	// 	  if ($alinea[$i]=="TRK" || $alinea[$i]=="HDG"){

	// 	      $angulo=$alinea[$i+1] + $correccion;
	//           $angradian=(-$angulo) * (M_PI/180);       

	//           if ($alinea[$i]=="TRK"){ 
	//           	  $distancia=-0.0166;	// 1 milla  
	// 		   }else{
	// 		   	  $distancia=1;
	// 		   }
	      

	// 	      if ($alinea[$i+2]=="UNTIL"){
	// 	      	// $distancia=$alinea[$i+3]; // luego de UNTIL (i+2)

	// 	      	$final_trk=$alinea[$i+3];
	// 	      	$millas=(strpos($final_trk, ".") ? true: false); // si tiene punto (.) es millas

	// 	      	if ($millas){
		      		
	// 	      	}
	// 	      	$coord=explode(" ",$last_pos);

	// 	      	$x1=$coord[0];
	// 	      	$y1=$coord[1];

	// 		    $x2=(cos($angradian) * $distancia) + $x1;
	// 		    $y2=(sin($angradian) * $distancia) + $y1;	

	// 	    	$vector_number++;
	// 	    	$fix2["vector_".$vector_number]=$x2." ".$y2;
	// 	    	$vectors["vector_".$vector_number]=$x2." ".$y2;

	// 	      }

	// 	      if ($alinea[$i+2]=="INTERCEPT"){
	// 	      		$angulo=$alinea[$i+2];   // 097
	// 	      		if ($alinea[$i+3]=="FROM"){

	// 	      		}
	// 	      	}
	// 	  } //if
	// 	}// for

// 		//============ PRUEBAS HARDCODE ==========
// //		RNW 13 TRK 132 UNTIL 418 HDG 040 INTERCEPT RADIAL 097 FROM FIX FDO HDG 359 INTERCEPT RADIAL 029 TO FIX 	KUKEN FIX KUKEN

// 		// $coord=explode(" ",trim($vectors["FDO"]));
// 		$coord=explode(" ",trim($fixs['R.13']));
// 		$x1=$coord[0];
// 		$y1=$coord[1];

// 		// si es TRK entonces DISTANCIA=negativa;
// 		// si es HDG entonces DISTANCIA=positiva;
// 		$trk=97;
// 		$correccion=-90;
// 		$DistanciaR13=10;

//         $angulo= $trk + $correccion;
//         $angradian=(-$angulo) * (M_PI/180);
// 	    $x2=(cos($angradian)*$DistanciaR13)+$x1;
// 	    $y2=(sin($angradian)*$DistanciaR13)+$y1;
		
// 		//========================================

	// 	$linea="";

	// 	foreach ($vectors as $key => $value) {
	// 		$linea.= ",".$value;
	// 	}

	// 	$linea="LINESTRING(".substr($linea, 1).")";

	// 	// var_dump($vectors);
	// 	// var_dump($linea);
		
	// 	// $ruta['vector']="LINESTRING($x1 $y1, $x2 $y2)";
	// 	$ruta['vector']=$linea;
	// 	echo json_encode($ruta);

	// } // function


	function vectores(){

		$data=$this->Model_procedures->get_airportdata("SABE");
		// var_dump($data);
		$fixs=$data['fixs'];
		// ///##################################################################################
		// //  MEJORA PARA INCLUIR TRK y HDG INTERCEPTING RADIALS
		// ///##################################################################################
		// SID KUKEN7
		$linea="RNW 31 HDG 311 UNTIL 800 HDG 040 UNTIL 15.5 FROM FIX PAL TURN RIGHT DIRECT FIX PAL";
		//  RNW 31 TRK 312 UNTIL 418 HDG 040 INTERCEPT RADIAL 029 TO FIX KUKEN FIX KUKEN

		$alinea=explode(" ",$linea);

		// echo ((2000*100)/111000)."<br>";  // 2000 metros = 1.80 grados mas o menos
		$recta=array();
		$v=0;
		$vector_number=0;
		$vectors=array();
		$last_pos="";
		$correccion=-90; // factor de correccion para llevar 0° a al norte en lugar de estar hacia el ESTE.

		for ($i=0; $i<count($alinea);$i++){

		  if ($alinea[$i]=='RNW'){
			$rnw=$alinea[$i+1];
			$vector["R.".$rnw]=trim($fixs["R.".$rnw]);	// origen: la pista

			$hdg=$data['runway_info'][$rnw]['hdg']*1;

			// // las sig sentencias son para desplazarse al final 
			// // de la pista de partida (se convierte de pies a millas)
			// $length=$data['runway_info'][$rnw]['length']*1;
			// $length=($length*0.000164579)*0.016;   // PIES a MILLAS y millas a grados

			//========INVERTIR PISTA ==========
			$letra="";
				if (strlen($rnw)>2){
					$letra=substr($rnw,2,1);
					$pista=substr($rnw,0,2)*10;  // agrego el cero a la derecha
				}else{
					$pista=$rnw*10;  // agrego el cero a la 
				}
				if ($pista<=180){
					$pista=$pista+180;
				}else
					$pista=$pista-180;
			// ya di vuela la pista
			$pista=$pista/10; // quito cero de la derecha
			if ($letra!=""){
				if ($letra=="L"){
					$letra="R";
				}
				if ($letra=="R"){
					$letra="L";
				}

				// solo para no olvidarme que esta la pista C (centro) 
				if ($letra=="C"){
					$letra="C";
				}
			}

			$lastpoint=$fixs["R.".$rnw];
			//=================================
			// echo "R.".$rnw."<br>";
			$lastpoint=$fixs["R.".$pista.$letra];
			$vector_number++;
			$vector["vector_".$vector_number]=$fixs["R.".$pista.$letra];

			$fixs["R.".$pista.$letra];
			$lastpoint=$fixs["R.".$pista.$letra];
			// echo "R.".$rnw." -> R.".$pista.$letra."<br>";

			// $lastpoint=$this->calcular_fix($fixs["R.".$pista.$letra],$hdg,$length);
		  } 

		  //========= TRK =====================		  

		  if ($alinea[$i]=="TRK" || $alinea[$i]=="HDG"){

		      $angulo=$alinea[$i+1]*1;
		      $angulo = $angulo;  // correcion para OSM (no se porque);

		      if ($alinea[$i+2]=="UNTIL"){
			      // si valor luego de TRK tiene punto "."
		          if (strpos($alinea[$i+3],".")){
		          	// hasta MILLAS desde ultimo FIX
		          	$distancia = ($alinea[$i+3]* 0.016);	// 0.016 => 1 milla o minuto de arco en grados  
		          }else{
		          	// hasta ALTITUD desde ultimo FIX
		          	$distancia = ($alinea[$i+3] / 100000)+0.02; // 0.005;	// 1nm
		          }
		          $p2=$this->calcular_fix($lastpoint,$angulo,$distancia);
		          // echo "$lastpoint , $angulo , $distancia <br>";

		          $vector_number++;
		          $vector["vector_".$vector_number]=$p2;
		          $lastpoint=$p2;
		      }
		   }

		   //========= HDG =====================
// $linea="RNW 13 TRK 132 UNTIL 418 HDG 040 INTERCEPT RADIAL 097 FROM FIX FDO HDG 359 INTERCEPT RADIAL 029 TO FIX KUKEN FIX KUKEN"		   

		   if ($alinea[$i]=="HDG"){
				$hdg=$alinea[$i+1]*1; // angulo
				// echo "HDG=> $hdg  ";
				if ($alinea[$i+2]=="INTERCEPT"){

					$radial=$alinea[$i+4]*1; // radial
					// echo "RADIAL=> $radial  ";
					$distancia=5; // 2 grados = 222 km 

					// PRIMER VECTOR HDG
					$p2_hdg=$this->calcular_fix($lastpoint,$hdg,$distancia);
					//$vector['last->'.$hdg]=$p2;
					$v++;
					$recta["v".$v]="$lastpoint,$p2_hdg"; // coord del vector

					// SEGUNDO VECTOR RADIAL FROM FIX
					$fijo=$fixs[$alinea[$i+7]];
					//$vector[$alinea[$i+7]]=$fijo;

					// $distancia=10;
					$p3=$this->calcular_fix($fijo,$radial,$distancia); 
					//$vector[$alinea[$i+7].'->'.$radial]=$p3;
					$v++;
					$recta["v".$v]="$fijo,$p3"; // coord del vector

					$interseccion=$this->intersection($lastpoint,$p2_hdg,$fijo,$p3);
					// echo $interseccion."<br>";
					$vector_number++;
					$vector['interseccion_'.$hdg.'->'.$radial]=$interseccion;
					$lastpoint=$interseccion;
				}
		   } // HDG

		   if ($alinea[$i]=="FIX" && $alinea[$i-1]!="TO" && $alinea[$i-1]!="FROM"){
		   		$vector_number++;
		   		$vector['vector_'.$vector_number]=$fixs[$alinea[$i+1]];
		   		$lastpoint=$fixs[$alinea[$i+1]];
		   }
		}// for


	 	 // var_dump($vector);


		$linea="";

		foreach ($vector as $key => $value) {
			$linea.= ",".$value;
		}

		$vectores="";
		foreach ($recta as $key => $value) {
			$vectores.="(".$value."),";
		}

		$vectores="";
		$linea="MULTILINESTRING(".$vectores."(".substr($linea, 1)."))";

		// var_dump($recta);
		// var_dump($linea);
		

		// $linea=",";
		// $linea.=$fixs['FDO'].",";
		// $linea.=$this->calcular_fix($fixs["FDO"],97,3);		
		// $linea="LINESTRING(".substr($linea, 1).")";

		// var_dump($vectors);
		// var_dump($linea);
		
		// $ruta['vector']="LINESTRING($x1 $y1, $x2 $y2)";



		$ruta['vector']=$linea;
		echo json_encode($ruta);

	} // function


	function calcular_fix($p1,$xang,$distancia){
		$coord=explode(" ",trim($p1));
		$x1=$coord[0]*1;
		$y1=$coord[1]*1;

		// si es TRK entonces DISTANCIA=negativa;
		// si es HDG entonces DISTANCIA=positiva;
		// $ang=97;
		$correccion=-90;
		// $DistanciaR13=10;
        $angulo= ($xang) + $correccion;
        $angradian=(-$angulo) * (M_PI/180);
	    $x2=(cos($angradian)*$distancia)+$x1;
	    $y2=(sin($angradian)*$distancia)+$y1;

// 	    echo "<br>>>>>>> x2= $x2  y2= $y2<br>";
	    return "$x2 $y2";

	}

	function intersection ($p1,$p2,$p3,$p4 ) {
		$p1=explode(" ",trim($p1));
		$p2=explode(" ",trim($p2));
		$p3=explode(" ",trim($p3));
		$p4=explode(" ",trim($p4));

		$ax1 = $p1[0];
		$ay1 = $p1[1];
		$ax2 = $p2[0];
		$ay2 = $p2[1];
		$bx1 = $p3[0];
		$by1 = $p3[1];
		$bx2 = $p4[0];
		$by2 = $p4[1];


	    $a1 = $ax1 - $ax2;
	    $a2 = $bx1 - $bx2;

	    $b1 = $ay1 - $ay2;
	    $b2 = $by1 - $by2;

	    $c = ( $a1 * $b2 ) - ( $b1 * $a2 );

	    if ( abs ( $c ) > 0.01 ) { // En caso de que haya interseccion

	        $a = ( $ax1 * $ay2 ) - ( $ay1 * $ax2 );
	        $b = ( $bx1 * $by2 ) - ( $by1 * $bx2 );

	        $x = ( $a * $a2 - $b * $a1 ) / $c;
	        $y = ( $a * $b2 - $b * $b1 ) / $c;

	    } else { // En caso de que no lo haya

	        $x = "";
	        $y = "";

	    }

	    return "$x $y";

	 }


 function gettour(){
      $archivo="F:/FSX/PMDG/NAVDATA/wpNavAPT.txt";
      $archi=fopen($archivo,"r+");

      $icao['PASC']='PASC';
      $icao['PAFA']='PAFA';
      $icao['CYXY']='CYXY';
      $icao['CYEG']='CYEG';
      $icao['KMSP']='KMSP';
      $icao['KDEN']='KDEN';
      $icao['KRND']='KRND';
      $icao['MMMX']='MMMX';
      $icao['MSLP']='MSLP';
      $icao['MPTO']='MPTO';
      $icao['SKCL']='SKCL';
      $icao['SEQM']='SEQM';
      $icao['SPJC']='SPJC';
      $icao['SCFA']='SCFA';
      $icao['SCEL']='SCEL';
      $icao['SAEZ']='SAEZ';
      $icao['SAVC']='SAVC';

      $salir=false;

      while (!feof($archi) && !$salir){
          $linea=fgets($archi);
          $aip=substr($linea,24,4);
          if (array_search($aip, $icao)!==false){
              $lon=trim(substr($linea,49,11));
              $lat=trim(substr($linea,39,10));
              $icao[$aip]="$lon $lat";
          }
      }
      fclose($archi);

      // var_dump($icao);

      $linea="";
      foreach ($icao as $key => $value) {
      	$linea.=",".$value;
      }

      if ($linea !=""){
      	$linea="LINESTRING(".substr($linea, 1).")";
      }else{
      	$linea="LINESTRING empty";
      }

      // var_dump($linea);

      return $linea;

}	



}