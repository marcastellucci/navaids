<?php

class Stars extends CI_Controller{



//#####################################################
//    FUNCIONES
//#####################################################
public function getRUNWAYS($icao){
	global $runways;

	$nomarchi="c:/ide/".$icao.".txt";
	$archi=fopen($nomarchi,"r+");

	$exit=false;
	$runways=array();
	$runway="";
	//------------------------------------
	// Leo linea por linea el archivo:
	//------------------------------------
	while (!feof($archi) && !$exit){

		$linea=trim(fgets($archi));
		//------------------------------------
		// si comienza seccion RNWS
		//------------------------------------
		if ($linea=="RNWS"){
			//------------------------------------
			// leo cada linea de RNWS
			//------------------------------------
			while (!feof($archi) and !$exit){

				$linea=trim(fgets($archi));
				//------------------------------------
				// si termina seccion RNWS, sale:
				//------------------------------------
				if ($linea=="ENDRNWS"){
					
					$exit=true;

				}else{
					//------------------------------------
					// MIENTRAS ESTE EN LA SECCION RNWS:
					//------------------------------------

					$alinea=explode(" ",$linea);
					if ($alinea[0]=="RNW"){
						$runways[]=$alinea[1];
					}

				} // if ($linea=="ENDRNWS"){

			} // while (!feof($archi) and !$exit){

		}  // if ($linea=="RNWS"){

	} // while todo el archivo

	fclose($archi);

	return $runways;
}


public function get_runwaySTARS(){
	global $stars;
	global $runways;

	// STARS 
	 $icao = $this->input->get('_icao');
	 $runway= $this->input->get('_runway');

	 $stars=$this->getSTARS($icao);
	 $selec_stars=array();
	 foreach ($stars as $key => $value) {
	 	if (array_search($runway, $value['runways'])!==false){
	 		$selec_stars[] = $key;
	 	}
	 }

	 echo json_encode($selec_stars);
}

public function get_starTRANSITIONS(){
	global $stars;

	 $icao = $this->input->get('_icao');
	 $starname = $this->input->get('_star');

	// TRANSITIONS	
	 $transiciones=array();

	 if (count($stars[$starname]['transitions'])>0){
	 	 $xtrans=$stars[$starname]['transitions'];
		 foreach ($xtrans as $key => $value) {
		 	$transiciones[]= $key;
		 }
	 }
	 
	 echo json_encode($transiciones);
}



//########## funciones internas ###################
function getSTARS($icao){

	$nomarchi="c:/ide/".$icao.".txt";
	$archi=fopen($nomarchi,"r+");

	$exit_stars=false;
	$stars=array();
	$starname="";
	//------------------------------------
	// Leo linea por linea el archivo:
	//------------------------------------
	while (!feof($archi) && !$exit_stars){

		$linea=trim(fgets($archi));
		//------------------------------------
		// si comienza seccion STARS
		//------------------------------------
		if ($linea=="STARS"){
			//------------------------------------
			// leo cada linea de STARS
			//------------------------------------
			while (!feof($archi) and !$exit_stars){

				$linea=trim(fgets($archi));
				//------------------------------------
				// si termina seccion STARS, sale:
				//------------------------------------
				if ($linea=="ENDSTARS"){
					
					$exit_stars=true;

				}else{
					//------------------------------------
					// MIENTRAS ESTE EN LA SECCION STARS:
					//------------------------------------

					$alinea=explode(" ",$linea);
					if ($alinea[0]=="STAR"){
						$starname=$alinea[1];
						$stars[$starname]['starfixs']=getvalues($alinea,'FIX');

					}
					if ($alinea[0]=="TRANSITION"){
						$transition_name=$alinea[1];
						$stars[$starname]['transitions'][$transition_name]['transitionfixs']=getvalues($alinea,'FIX');
						
					}
					if ($alinea[0]=="RNW"){
						$stars[$starname]['runways']=getvalues($alinea,'RNW');
					}				

				} // if ($linea=="ENDSTARS"){

			} // while (!feof($archi) and !$exit_stars){

		}  // if ($linea=="STARS"){

	} // while todo el archivo

	fclose($archi);

	return $stars;
}



function getvalues($array_linea,$clave){
	$result=array();
	$contiene_clave=array_search($clave, $array_linea);
	if ($contiene_clave!== false){	

		for ($i=0;$i+1 < count($array_linea);$i++){
			if ($array_linea[$i]==$clave){
				$skip=1;
				if ($array_linea[$i+$skip]=="OVERFLY"){
					$skip=2;
				}
				$result[]=$array_linea[$i+$skip];
			}
		}
	}

	// retorna array con los FIX o lo que se busque en CLAVE:
	return $result;
}


} 


