<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */

	public function __construct(){
		parent::__construct();
		$this->load->model('Model_consultas');
	}

	public function index()
	{
		// $data['select_pistas'] = $this->Model_consultas->buscar_pistas("KATL");
		// $data['select_sids'] = $this->Model_consultas->buscar_sids("KATL");
		// echo "<br>######################################<br>";
		// var_dump($data['select_pistas']);
		// $data['selec_airports']=$this->Model_consultas->get_airports();

		//  $icao    = "KJFK";
		//  $sidname = "";
		//  $rnw     = "";
		//  $tname   = "";

		//  $aipdata = $this->Model_consultas->get_airportdata($icao);
		//  $resu = $this->Model_consultas->get_infototal($icao);

		//  // recorro los arrays devueltos en "get_airportdata" y los paso a $RESU;
		//  foreach ($aipdata['runway'] as $irunway  => $nropista) {
		//  	# code...
		//  	// almaceno coord de cada pista en arreglo de FIJOS
		//  	$resu['fixes'][$irunway]=$aipdata['runway'][$irunway];
		//  }
		//  $resu['centro']=$aipdata['centro'];
		 
		// foreach ($resu['sid'] as $key => $value) {
		// 	var_dump($resu['sid'][$key]);
		// }
		
		//  $linestring="LineString(";

		//  foreach ($resu['sid'][$sidname]['rnw'] as $irunway => $nropista) {
		 	
		//  	if ($nropista==$rnw){
		//  		// echo "SID = $sidname <br>";
		//  		// echo "---- FIX1: <br>"; 		 		
		//  		if (count($resu['sid'][$sidname]['fix1'][$rnw])>0){		 			
		// 	 		foreach ($resu['sid'][$sidname]['fix1'][$rnw] as $key_fix1 => $value_fix1) {
		// 	 			// aca falla en SAME=  sid:ESITO3A rnw=36
		// 	 			//echo "-------- F1: $value_fix1 <br>";
		// 	 			// $linestring=$linestring.$value_fix1.",";
		// 	 			$linestring=$linestring.$resu['fixes'][$value_fix1].",";
			 			
		// 	 		}
		// 	 	}
			 	
		// 	 	if (count($resu['sid'][$sidname]['fix2'])>0){
		// 	 		foreach ($resu['sid'][$sidname]['fix2'] as $key_fix2 => $value_fix2) {
		// 	 			// echo "------------------ F2: $value_fix2 <br>";
		// 	 			// $linestring=$linestring.$value_fix2.",";
		// 	 			$linestring=$linestring.$resu['fixes'][$value_fix2].",";
		// 	 		}
		// 	 	}
		//  		if ($tname!=="" && $resu['sid'][$sidname]['transitions'][$tname]>0) {
		//  			// echo "------------ TRANSICIONES DE: $tname <br>";
		//  			foreach ($resu['sid'][$sidname]['transitions'][$tname] as $key_transi => $value_transi) {
		//  				// echo "------------------------------------ $value_transi <br>";
		//  				// $linestring=$linestring.$value_transi.",";
		//  				$linestring=$linestring.$resu['fixes'][$value_transi].",";
		//  			}
		//  		}

		//  	}
		//  }
		//  $linestring=substr($linestring, 0, strlen($linestring)-1).")";
		//  echo "<br>".$linestring;
		 

		$data['application_name']="DEPARTURE-ARRIVAL PROCEDURES";
		$data['airac_version']="2019-08";
		// $this->load->view('inicio',$data);
		$this->load->view('view_procedures',$data);
	}
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */