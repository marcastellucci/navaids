
<?php 


class Consultas extends CI_Controller {

	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	
	public function __construct(){
		parent::__construct();
		$this->load->model('Model_consultas');
	}


	public function buscar_pistas(){

		 $icao = $this->input->get('_icao');

		// $this->output->enable_profiler(TRUE);

		$pistas = $this->Model_consultas->buscar_pistas($icao);  // geojson string
		// $resultado = $this->Model_consultas->proceso_navaids($icao);	

		echo json_encode($pistas);    //geojson string
	}

	public function buscar_sid_o_star(){
		$icao = $this->input->get('_icao');
		$sid_star=$this->input->get('_sid_star');
        $pista = $this->input->get('_pista');
		$resu = $this->Model_consultas->buscar_procedimiento($icao,$sid_star,$pista);

		echo json_encode($resu);
	}


	public function get_procedimiento(){

		$icao = $this->input->get('_icao');
		$resu = $this->Model_consultas->get_fixes($icao);

		echo json_encode($resu);
	}

	public function get_transiciones(){
		$icao=$this->input->get('_icao');
		$sid_o_star=$this->input->get('_sid_o_star');
		$pista=$this->input->get('_pista');
		$sidstar_name = $this->input->get('_sidstar_name');

		$resu = $this->Model_consultas->get_transiciones($icao,$sid_o_star,$sidstar_name,$pista);
		echo json_encode($resu);
	}

// ==============================================================
//  GET_AIRPORTDATA
//                   devuelve SID o STAR, transiciones, etc.
//   devuelve de todo.
//===============================================================

    function get_airportdata(){

        $salir=false;
        $stars=[];
        $oldstar="";
        while (!feof($archi) && !$salir){

            $starfix=[];
            $trans=[];
            $transfix=[];
            $rw="";

            $linea=fgets($archi); 
            $exp=split($linea,' ');
            if ($exp(0)=='STAR')
                $starts[$exp(1)]=

            while (!feof($archi) && )
            
            

        }

        =fclose($archi);

    }


    function get_stars(){

         $icao= $this->input->get('_icao');       
         $rnw= $this->input->get('_runway');

         $procedimiento= $this->input->get('_procedimiento');
         $sidstar_name= $this->input->get('_proc_name');
         
         $tname= $this->input->get('_transition');
         

         $aipdata=$this->Model_consultas->get_airportdata($icao);
         $resu=$this->Model_consultas->get_infototal($icao,$procedimiento);

         // recorro los arrays devueltos en "get_airportdata" y los paso a $RESU;
         foreach ($aipdata['runway'] as $irunway  => $nropista) {
            # code...
            // almaceno coord de cada pista en arreglo de FIJOS
            $resu['fixes'][$irunway]=$aipdata['runway'][$irunway];
         }
         $resu['centro']=$aipdata['centro'];
         
         $linestring="LineString(";

         //-------------------------------------------------------------------------------
         // GeoJSON para los nuevos FIXs pertenecientes al LINESTRING de la RUTA SID
         $geojson = array(
                'type'      => 'FeatureCollection',
                'features'  => array()   // aca luego se asignara la coleccion de puntos
            );
            $id=0;       
         //-------------------------------------------------------------------------------

         foreach ($resu['procedimiento'][$sidstar_name]['rnw'] as $irunway => $nropista) {
            
            if ($nropista==$rnw){
                // echo "SID = $sidname <br>";
                // echo "---- FIX1: <br>";              
                if (count($resu['procedimiento'][$sidstar_name]['fix1'][$rnw])>0){                 
                    foreach ($resu['procedimiento'][$sidstar_name]['fix1'][$rnw] as $key_fix1 => $value_fix1) {
                        // aca falla en SAME=  sid:ESITO3A rnw=36
                        //echo "-------- F1: $value_fix1 <br>";
                        // $linestring=$linestring.$value_fix1.",";
                        $linestring=$linestring.$resu['fixes'][$value_fix1].",";
                    //====================================                    
                    //       G E O J S O N
                    //====================================
                    $cad=$linestring.$resu['fixes'][$value_fix1];  // coordenada en formato string:  LON LAT  (espacio como separad)
                    $cad=explode(" ",$cad);
                    $feature = array(
                            'id' => $id,
                            'type' => 'Feature', 
                            'geometry' => array(
                                'type' => 'Point',
                                # Pass Longitude and Latitude Columns here
                                // multiplico x 1 pq sino la coordenada 
                                // se genera como string, en cambio
                                // asi, se genera como "numerico"
                                'coordinates' => array($cad[0]*1,$cad[1]*1)     // coordenada 
                            ),
                            # Pass other attribute columns here
                            'properties' => array(
                                'fixname' => $key_fix1
                                )
                            );
                        # Add feature arrays to feature collection array
                        array_push($geojson['features'], $feature);                                
                    //====================================                      
                        
                    }
                }
                
                if (count($resu['procedimiento'][$sidstar_name]['fix2'])>0){
                    foreach ($resu['procedimiento'][$sidstar_name]['fix2'] as $key_fix2 => $value_fix2) {
                        // echo "------------------ F2: $value_fix2 <br>";
                        // $linestring=$linestring.$value_fix2.",";
                        $linestring=$linestring.$resu['fixes'][$value_fix2].",";
                        //====================================                    
                        //       G E O J S O N
                        //====================================
                        $cad=$linestring.$resu['fixes'][$value_fix2];  // coordenada en formato string:  LON LAT  (espacio como separad)
                        $cad=explode(" ",$cad);
                        $feature = array(
                                'id' => $id,
                                'type' => 'Feature', 
                                'geometry' => array(
                                    'type' => 'Point',
                                    # Pass Longitude and Latitude Columns here
                                    // multiplico x 1 pq sino la coordenada 
                                    // se genera como string, en cambio
                                    // asi, se genera como "numerico"
                                    'coordinates' => array($cad[0]*1,$cad[1]*1)     // coordenada 
                                ),
                                # Pass other attribute columns here
                                'properties' => array(
                                    'fixname' => $key_fix2
                                    )
                                );
                            # Add feature arrays to feature collection array
                            array_push($geojson['features'], $feature);                                
                        //====================================                              
                    }
                }
                if ($tname!=="" && $resu['procedimiento'][$sidstar_name]['transitions'][$tname]>0) {
                    // echo "------------ TRANSICIONES DE: $tname <br>";
                    foreach ($resu['procedimiento'][$sidstar_name]['transitions'][$tname] as $key_transi => $value_transi) {
                        // echo "------------------------------------ $value_transi <br>";
                        // $linestring=$linestring.$value_transi.",";
                        $linestring=$linestring.$resu['fixes'][$value_transi].",";
                        //====================================                    
                        //       G E O J S O N
                        //====================================
                        $cad=$resu['fixes'][$value_transi];  // coordenada en formato string:  LON LAT  (espacio como separad)
                        $cad=explode(" ",$cad);
                        $feature = array(
                                'id' => $id,
                                'type' => 'Feature', 
                                'geometry' => array(
                                    'type' => 'Point',
                                    # Pass Longitude and Latitude Columns here
                                    // multiplico x 1 pq sino la coordenada 
                                    // se genera como string, en cambio
                                    // asi, se genera como "numerico"
                                    'coordinates' => array($cad[0]*1,$cad[1]*1)     // coordenada 
                                ),
                                # Pass other attribute columns here
                                'properties' => array(
                                    'fixname' => $key_transi
                                    )
                                );
                            # Add feature arrays to feature collection array
                            array_push($geojson['features'], $feature);                                
                        //====================================                              
                    }
                }

            }
         }
         $linestring=substr($linestring, 0, strlen($linestring)-1).")";
         $resu['procedimiento'][$sidstar_name]['linestring']=$linestring;
         $resu['procedimiento'][$sidstar_name]['geojson_fixs']=$geojson;

         //arreglo con los nombres de las SIDS o STARS:
         $resu['procedimiento']['only_procnames']=array();
         foreach ($resu['procedimiento'][$sidstar_name] as $key => $value) {
            $resu['procedimiento']['only_procnames'][]=$key;    
         }
        //------------------------------

         //arreglo con las transiciones:
         $resu['procedimiento'][$sidstar_name]['only_transitions']=array();
         foreach ($resu['procedimiento'][$sidstar_name]['transitions'] as $key => $value) {
            $resu['procedimiento'][$sidstar_name]['only_transitions'][]=$key;    
         }
        //------------------------------

         $geojson_string=json_encode($resu);
         echo $geojson_string;
    }



}	// end controlador

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */