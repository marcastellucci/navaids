<?php

class Controller_sidstars extends CI_Controller{

// para usar variable global
// dentro de la clase (PERO NO ME FUNCIONA BIEN)
private $stars;	

public function __construct() {
    parent::__construct();
    $this->stars = array();   
}

//#####################################################
//    FUNCIONES
//#####################################################
public function getRUNWAYS(){

	$icao=$this->input->get('_icao');

	$nomarchi="f:/fsx/pmdg/sidstars/".$icao.".txt";
	$archi=fopen($nomarchi,"r+");

	$exit=false;
	$runways=array();
	$runways[]="seleccione...";
	$runway="";
	//------------------------------------
	// Leo linea por linea el archivo:
	//------------------------------------
	while (!feof($archi)) {

		$linea=trim(fgets($archi));
		//------------------------------------
		// si comienza seccion RNWS
		//------------------------------------
		if ($linea=="RNWS"){
			//------------------------------------
			// leo cada linea de RNWS
			//------------------------------------
			$exit=false;
			while (!feof($archi) and !$exit){

				$linea=trim(fgets($archi));
				//------------------------------------
				// si termina seccion RNWS, sale:
				//------------------------------------
				if ($linea=="ENDRNWS"){
					
					$exit=true;

				}else{
					//------------------------------------
					// MIENTRAS ESTE EN LA SECCION RNWS:
					//------------------------------------

					$alinea=explode(" ",$linea);
					if ($alinea[0]=="RNW"){
						$runways[$alinea[1]]=$alinea[1];
					}

				} // if ($linea=="ENDRNWS"){

			} // while (!feof($archi) and !$exit){

		}  // if ($linea=="RNWS"){			

	} // while todo el archivo

	fclose($archi);

	return $runways;
}


public function get_SIDoSTAR(){

	// STARS 
	 $icao = $this->input->get('_icao');
	 $runway= $this->input->get('_runway');
	 $tipo_proc=$this->input->get('_tipo_proc');

	 if ($tipo_proc=="SID"){
	 	$proc=$this->getSIDS($icao);
	 }else{
	 	$proc=$this->getSTARS($icao);
	 }
	 $selec_stars=array();
	 
	 if (count($proc)>0){
	 	 $selec_stars[]="seleccione...";		 
		 foreach ($proc as $key => $value) {
		 	if (array_search($runway, $value['runways'])!==false){
		 		$selec_stars[] = $key;
		 	}
		 }
	}else{
		$selec_stars[]="vacio";
	}

	 echo json_encode($selec_stars);
}

public function get_TRANSITIONS(){

	 $icao = $this->input->get('_icao');
	 $name = $this->input->get('_proc_name');
	 $runway = $this->input->get('_runway');
	 $tipo_proc = $this->input->get('_procedimiento');

	 $route="";
	 $proc=array();
	 if ($tipo_proc=="SID"){
	 	$proc=$this->getSIDS($icao);	
	 	$route=$this->getPATHSID($icao,$proc[$name]['sidfixs'],'SID',$runway);	// LINESTRING() well know text
	 }else{
	 	$proc=$this->getSTARS($icao);
	 	$route=$this->getPATH($icao,$proc[$name]['starfixs'],'STAR'); // LINESTRING() well know text
	 }

	// TRANSITIONS	
	 $transiciones=array();

	 if (count($proc[$name]['transitions'])>0){
		$transiciones[]="seleccione...";	 	
	 	 $xtrans=$proc[$name]['transitions'];
		 foreach ($xtrans as $key => $value) {
		 	$transiciones[]= $key;
		 }
	 }else{
	 	$transiciones[]="sin transiciones";
	 }
	 
	 // var_dump($proc);
	 $resultado['transition_list']=$transiciones;
	 $resultado['route']=$route;
	 // echo json_encode($transiciones);
	 echo json_encode($resultado);
}


//------------------------------------------------------
// LINESTRING con los FIJOS INICIALES 
// O FINALES DE LOS SIDs o STARs
// ejemplo:
// 	 SID BITAG2 RNW 36 HDG 001 UNTIL 2350 FIX BITAG AT OR ABOVE 6500
//   STAR DILI1A FIX DILIX AT OR ABOVE 5000 FIX NU426 
//------------------------------------------------------
function getPATH($icao,$proc_fixes, $tipo_proc){

	$afixes=$this->get_fixes($icao);

	$wkt="LINESTRING(";

	foreach ($proc_fixes as $key => $value) {
		$wkt.=$afixes['fixes_coord'][$value].",";
	}
	$wkt=substr($wkt,0, strlen($wkt)-1);
	$wkt.=")";

	return $wkt;

}

function getPATHSID($icao,$proc_fixes, $tipo_proc,$xrunway){

	$ini_fix = $this->get_runwaylocation($icao,$xrunway);

	$afixes=$this->get_fixes($icao);


	$wkt="LINESTRING(";

	// coloco coordenada de la pista INI_FIX
	$wkt.=$ini_fix;
	foreach ($proc_fixes as $key => $value) {
		$wkt.=",".$afixes['fixes_coord'][$value];
	}
	$wkt=substr($wkt,0);
	$wkt.=")";

	return $wkt;

}


public function get_fullroute(){

	 $icao = $this->input->get('_icao');
	 $name = $this->input->get('_proc_name');
	 $runway = $this->input->get('_runway');
	 $tipo_proc = $this->input->get('_procedimiento');
	 $transition=$this->input->get('_transition');

	 $route="";
	 $proc=array();
	 if ($tipo_proc=="SID"){
	 	$proc=$this->getSIDS($icao);
	 	$route=$this->getFULLPATH($icao,$proc[$name],$transition,'SID');
	 }else{
	 	$proc=$this->getSTARS($icao);
	 	// var_dump($proc[$name]['transitions'][$transition]);
	 	$route=$this->getFULLPATH($icao,$proc[$name],$transition,'STAR');
	 }

	
 	 $resultado['route']=$route;
	 // echo json_encode($transiciones);
	 echo json_encode($resultado);
}

function getFULLPATH($icao,$proc, $transit_name, $tipo_proc){

	$afixes=$this->get_fixes($icao);

	$wkt="LINESTRING(";

	// var_dump($proc);
	$fijos1="";
	$fijos2="";
	foreach ($proc['transitions'][$transit_name] as $key => $value) {
		$fijos1.=$afixes['fixes_coord'][$value].",";
	}

	if ($tipo_proc=="SID"){
		$fijos="sidfixs";
	}else{
		$fijos="starfixs";
	}

	foreach ($proc[$fijos] as $key => $value) {
		$fijos2.=$afixes['fixes_coord'][$value].",";
	}

	// si tipo=SID, sidfixs+fijos
	if ($tipo_proc=="SID"){		
		$wkt.=$fijos2.$fijos1;
	}else{
		// si tipo=STAR, fijos+starfixs
		$wkt.=$fijos1.$fijos2;
	}
	

	$wkt=substr($wkt,0, strlen($wkt)-1);
	$wkt.=")";

	return $wkt;

}

//########## funciones internas ###################

function getSIDS($icao){

	$nomarchi="f:/fsx/pmdg/sidstars/".$icao.".txt";
	$archi=fopen($nomarchi,"r+");

	$exit_stars=false;
	$xstars=array();
	$starname="";
	//------------------------------------
	// Leo linea por linea el archivo:
	//------------------------------------
	while (!feof($archi) && !$exit_stars){

		$linea=trim(fgets($archi));
		//------------------------------------
		// si comienza seccion SIDS
		//------------------------------------
		if ($linea=="SIDS"){
			//------------------------------------
			// leo cada linea de SIDS
			//------------------------------------
			while (!feof($archi) and !$exit_stars){

				$linea=trim(fgets($archi));
				//------------------------------------
				// si termina seccion SIDS, sale:
				//------------------------------------
				if ($linea=="ENDSIDS"){
					
					$exit_stars=true;

				}else{
					//------------------------------------
					// MIENTRAS ESTE EN LA SECCION SIDS:
					//------------------------------------

					$alinea=explode(" ",$linea);
					if ($alinea[0]=="SID"){

						$starname=$alinea[1];
						// luego los demás fijos de la salida
						$xstars[$starname]['sidfixs']=$this->getvalues($alinea,'FIX');
						// primero el fijo de la pista de salida
						$contiene_rnw=array_search("RNW",$alinea);
						if ($contiene_rnw !== false){
							$xstars[$starname]['runways']=$this->getvalues($alinea,'RNW');
						}		
					}
					if ($alinea[0]=="TRANSITION"){
						$transition_name=$alinea[1];
						$xstars[$starname]['transitions'][$transition_name]=$this->getvalues($alinea,'FIX');
						
					}
					// if ($alinea[0]=="RNW"){
					// 	$xstars[$starname]['runways']=$this->getvalues($alinea,'RNW');
					// }				

				} // if ($linea=="ENDSTARS"){

				if (!isset($xstars[$starname]['transitions'])){
					$xstars[$starname]['transitions']=array();
				}

			} // while (!feof($archi) and !$exit_stars){

		}  // if ($linea=="STARS"){

	} // while todo el archivo

	fclose($archi);

	return $xstars;
}

function getSTARS($icao){

	$nomarchi="f:/fsx/pmdg/sidstars/".$icao.".txt";
	$archi=fopen($nomarchi,"r+");

	$exit_stars=false;
	$xstars=array();
	$starname="";
	//------------------------------------
	// Leo linea por linea el archivo:
	//------------------------------------
	while (!feof($archi) && !$exit_stars){

		$linea=trim(fgets($archi));
		//------------------------------------
		// si comienza seccion STARS
		//------------------------------------
		if ($linea=="STARS"){
			//------------------------------------
			// leo cada linea de STARS
			//------------------------------------
			while (!feof($archi) and !$exit_stars){

				$linea=trim(fgets($archi));
				//------------------------------------
				// si termina seccion STARS, sale:
				//------------------------------------
				if ($linea=="ENDSTARS"){
					
					$exit_stars=true;

				}else{
					//------------------------------------
					// MIENTRAS ESTE EN LA SECCION STARS:
					//------------------------------------

					$alinea=explode(" ",$linea);
					if ($alinea[0]=="STAR"){
						$starname=$alinea[1];
						$xstars[$starname]['starfixs']=$this->getvalues($alinea,'FIX');

					}
					if ($alinea[0]=="TRANSITION"){
						$transition_name=$alinea[1];
						$xstars[$starname]['transitions'][$transition_name]=$this->getvalues($alinea,'FIX');
						
					}
					if ($alinea[0]=="RNW"){
						$xstars[$starname]['runways']=$this->getvalues($alinea,'RNW');
					}				

				} // if ($linea=="ENDSTARS"){

				if (!isset($xstars[$starname]['transitions'])){
					$xstars[$starname]['transitions']=array();
				}

			} // while (!feof($archi) and !$exit_stars){

		}  // if ($linea=="STARS"){

	} // while todo el archivo

	fclose($archi);

	return $xstars;
}





function getvalues($array_linea,$clave){
	$result=array();
	$contiene_clave=array_search($clave, $array_linea);
	if ($contiene_clave!== false){	

		for ($i=0;$i+1 < count($array_linea);$i++){
			if ($array_linea[$i]==$clave){
				$skip=1;
				if ($array_linea[$i+$skip]=="OVERFLY"){
					$skip=2;
				}
				$result[$array_linea[$i+$skip]]=$array_linea[$i+$skip];
			}
		}
	}

	// retorna array con los FIX o lo que se busque en CLAVE:
	return $result;
}

function get_fixes($icao){

    $afixes=array();

    $archivo='f:/fsx/pmdg/sidstars/'.$icao.'.txt';
    if (!file_exists($archivo)){
        $geojson = "vacio";
        return $geojson;
    }

    $archi=fopen($archivo,'r');

    $salir=false;
    $linea=trim(fgets($archi));
    $arre_linea=explode(" ",$linea);
    $multipoint="";
    $multipoint_GeoJSON="";
    $sql="";
    $lon="";$lat="";

    
   $geojson = array(
        'type'      => 'FeatureCollection',
        'features'  => array()
    );
    $id=0;

	    while (!feof($archi) and $arre_linea[0]!=="ENDFIXES"){        
	        
	        if (count($arre_linea)>1){
	                $fix_name=$arre_linea[1];
	                $lon="";
	                $lat="";
	                if ($arre_linea[0]=="FIX"){
	                    if ($arre_linea[3]=="S")
	                        $lat="-";
	                    if ($arre_linea[6]=="W")
	                        $lon="-";

	                    $lat.=$arre_linea[4]+substr(($arre_linea[5]/60),1);
	                    $lon.=$arre_linea[7]+substr(($arre_linea[8]/60),1);
	                    $afixes[$arre_linea[1]] = "$lon $lat";

	                    //====================================                    
	                    //       G E O J S O N
	                    //====================================

	                    $feature = array(
	                            'id' => $id,
	                            'type' => 'Feature', 
	                            'geometry' => array(
	                                'type' => 'Point',
	                                # Pass Longitude and Latitude Columns here
	                                // multiplico x 1 pq sino la coordenada 
	                                // se genera como string, en cambio
	                                // asi, se genera como "numerico"
	                                'coordinates' => array($lon*1,$lat*1)
	                            ),
	                            # Pass other attribute columns here
	                            'properties' => array(
	                                'fixname' => $fix_name
	                                )
	                            );
	                        # Add feature arrays to feature collection array
	                        array_push($geojson['features'], $feature);                                
	                    //====================================

	                }
	        }
	        $linea=trim(fgets($archi));        
	        $arre_linea=explode(" ",$linea);

	        $id++;

	    }   // fin while ENDRNWS

    fclose($archi);

    $resultado['geojson']=$geojson;
    $resultado['fixes_coord']=$afixes;
    // return $geojson;
    return $resultado;
}


function get_airportdata(){
    
    $icao=$this->input->get('_icao');

    $resu=array();
    $centro=array();
    $xarchi=fopen("F:/FSX/PMDG/NAVDATA/wpNavAPT.txt","r+");
    $salir=false;
    $airport_name="";
    $cant_pistas=0;
    $centro['lon']=0;
    $centro['lat']=0;
    while (!feof($xarchi) && !$salir){
        $linea=trim(fgets($xarchi));
        if (substr($linea,24,4)===$icao){

        	$cant_pistas++;

			// sumo las coord de las pistas encontradas para promediar
			// su lonlat y asi poder determinar un centro de pistas o aip
            $centro['lon']+=substr($linea,50,10)*1;	
            $centro['lat']+=substr($linea,39,10)*1;

            $runway=trim(substr($linea,28,3));
            // obtengo coord de la pista encontrada
            $resu['runway_location'][$runway]=substr($linea,50,10)." ".substr($linea,39,10);
            $airport_name=substr($linea,0,24);
        }
    }
    fclose($xarchi);

    // promedio para centro de airport
    $centro['lon']=($centro['lon'] / $cant_pistas);
    $centro['lat']=($centro['lat'] / $cant_pistas);

    // OJO: agrego al array RESU el array con el CENTRO 
    $resu['centro']=$centro;
    $resu['airport_name']=$airport_name;
    $resu['runways_list']=$this->getRUNWAYS($icao);


    $fijos=$this->get_fixes($icao);  
    $resu['fixes']=$fijos['geojson'];   // retorna un geojson con los FIX y atributo nombre para motrar label
    // $resu['fixes_list']=$fijos['fixes_list'];   // array con las coord de cada FIX

    // var_dump($resu);

    echo json_encode($resu);

}


function get_runwaylocation($icao, $xrunway){
    
    $resu="";
    $centro=array();
    $xarchi=fopen("F:/FSX/PMDG/NAVDATA/wpNavAPT.txt","r+");
    $salir=false;
    $airport_name="";
    $cant_pistas=0;
    $centro['lon']=0;
    $centro['lat']=0;
    while (!feof($xarchi) && !$salir){
        $linea=trim(fgets($xarchi));
        if (substr($linea,24,4)===$icao && trim(substr($linea,28,3))===trim($xrunway)){

            $runway=trim(substr($linea,28,3));
            // obtengo coord de la pista encontrada
            $resu=substr($linea,50,10)." ".substr($linea,39,10);
            $salir=true;
        }
    }
    fclose($xarchi);

    return $resu;

}


} 


