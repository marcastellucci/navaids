<?php

Class Model_procedures extends CI_Model{
	

	// ==========================================
	// INFORMACION DEL AEROPUERTO:
	// Respuesta: JSON con:
	//			- Array Assoc: LON LAT del centro de aeropuerto
	//			- Array Assoc: LISTA DE PISTAS (para el combo)
	//			- String: Nombre del Aeropuerto
	// ==========================================
	function get_airportdata($icao,$xrunway=false){

		$archivo="F:/FSX/PMDG/NAVDATA/wpNavAPT.txt";
	    $archi=fopen($archivo,"r+");
	    $salir=false;
	    $cant_pistas=0;
	    $centro=array();
	    $centro['lon']=0;
	    $centro['lat']=0;	  
	    $runway_list=array(); 
	    $runway_info=array(); 
	    $runway_list['0']="seleccione...";

	    while (!feof($archi) && !$salir){
	    	$linea=fgets($archi);

	       if (substr($linea,24,4)===$icao){

	        	$cant_pistas++;

				// sumo las coord de las pistas encontradas para promediar
				// su lonlat y asi poder determinar un centro de pistas o aip
	            $centro['lon']+=substr($linea,49,10)*1;	
	            $centro['lat']+=substr($linea,39,10)*1;

	            $runway=trim(substr($linea,28,3));

	            $runway_list[$runway]=$runway;
	            $runway_info[$runway]['hdg']=trim(substr($linea,36,3));
	            $runway_info[$runway]['length']=trim(substr($linea,31,5));

	            // obtengo coord de la pista encontrada
	            $resu['runway_location'][$runway]=substr($linea,49,10)." ".substr($linea,39,10);
	            $airport_name=substr($linea,0,24);
	        }
	    }
	    fclose($archi);

	    $centro['lon']=($centro['lon'] / $cant_pistas);
	    $centro['lat']=($centro['lat'] / $cant_pistas);

	    $resultado['airport_name']=$airport_name;
	    $resultado['centro']=$centro;
	    $resultado['runway_list']=$runway_list;
	    $resultado['runway_info']=$runway_info;
	    $resultado['runway_location']=$resu['runway_location'];


	    // obtengo todos las coord de FIJOS incluidas las coord de pistas
	    $data=$this->get_fixs($icao,$resultado['runway_location']);
       	$resultado['approaches_list']=$this->get_approaches_list($icao);

	    $resultado['geojson_fixs']=$data['geojson_fixs'];
	    $resultado['fixs']=$data['fixs'];

	    return $resultado;

	}


	function get_fixs($icao,$runway_location=false){
		$archivo="F:/FSX/PMDG/SIDSTARS/".$icao.".txt";
	    $archi=fopen($archivo,"r+");

	    $fixs=array();

	    // primero incorporo los fijos de las pistas
	    // y como identificado uso R. para indicar Rwy
	    foreach ($runway_location as $key => $value) {
	    	$fixs["R.".$key]=$value;
	    }
	    
	    //..............................................
	    // geojson string para los fijos del mapa
	    //..............................................
		$geojson = array(
	        'type'      => 'FeatureCollection',
	        'features'  => array()
	    );
	    // id para los features
		$id=0;
		$lon="";$lat="";
		//..............................................
	    $salir=false;
	    while (!feof($archi) and !$salir){        
			$linea=trim(fgets($archi));
			$arre_linea=explode(" ",$linea);	    	

			if ($linea=="ENDFIXES"){
				$salir=true;
			}
	        
	        if (count($arre_linea)>1){
	                $fix_name=$arre_linea[1];
	                $lon="";
	                $lat="";
	                if ($arre_linea[0]=="FIX"){
	                    if ($arre_linea[3]=="S")
	                        $lat="-";
	                    if ($arre_linea[6]=="W")
	                        $lon="-";

	                    $lat.=$arre_linea[4]+substr(($arre_linea[5]/60),1);
	                    $lon.=$arre_linea[7]+substr(($arre_linea[8]/60),1);
	                    $fixs[$fix_name] = "$lon $lat";

	                    //====================================                    
	                    //       G E O J S O N
	                    //====================================

	                    $feature = array(
	                            'id' => $id,
	                            'type' => 'Feature', 
	                            'geometry' => array(
	                                'type' => 'Point',
	                                # Pass Longitude and Latitude Columns here
	                                // multiplico x 1 pq sino la coordenada 
	                                // se genera como string, en cambio
	                                // asi, se genera como "numerico"
	                                'coordinates' => array($lon*1,$lat*1)
	                            ),
	                            # Pass other attribute columns here
	                            'properties' => array(
	                                'fixname' => $fix_name
	                                )
	                            );
	                        # Add feature arrays to feature collection array
	                        array_push($geojson['features'], $feature);                                
	                    //====================================

	                }
	        }

	        $id++;

	    }   // fin while ENDRNWS

    	fclose($archi);

	    $resu['geojson_fixs']=$geojson;
	    $resu['fixs']=$fixs;

	    return $resu;
	}	

	// ==========================================
	// INFO de los SID y STAR
	// Respuesta: JSON con:
	//			- Array Assoc: LISTA de SID o STAR (para el combo de procedimientos)
	// ==========================================	
	function get_procedures($icao,$runway,$tipo_proc,$proc_name=false,$xtransition_name=false){

		$nomarchi="f:/fsx/pmdg/sidstars/".$icao.".txt";
		$archi=fopen($nomarchi,"r+");

		$exit=false;
		$proc=array();
		$name="";
		if ($tipo_proc=="SID"){
			$ini_proc="SIDS";
			$fin_proc="ENDSIDS";
		}else{
			$ini_proc="STARS";
			$fin_proc="ENDSTARS";
		}

		$old="";
		$old_transition="";
		$name="";
		$fix1=array();
		$fix2=array();
		$fix3=array();
		$fix4=array();
		// $resu['procedure_list']=array();

		$transitions=array();
		$transition_name="";
		$resu=array();
		$resu['procedure_list']=array();
		$resu['transition_list']=array();
		$listar=false;


		$fix_rwy["R.".$runway]="R.".$runway;
		$resu=array();
		//------------------------------------
		// Leo linea por linea el archivo:
		//------------------------------------
		while (!feof($archi) && !$exit){

			$linea=trim(fgets($archi));
			//------------------------------------
			// si comienza seccion SIDS o STARS
			//------------------------------------
			if ($linea==$ini_proc){		// SIDS o STARS
				//------------------------------------
				// leo cada linea de SIDS o STAR
				//------------------------------------
				while (!feof($archi) and !$exit){

					$linea=trim(fgets($archi));
					$alinea=explode(" ",$linea);
					//------------------------------------
					// si termina seccion ENDSIDS, sale:
					//------------------------------------
					if ($linea==$fin_proc){  // ENDSIDS o ENDSTARS
						
						$exit=true;

					}else{
							//------------------------------------
							// Mientras esté en la sección SIDS o STARS:
							//------------------------------------
						// if ($tipo_proc=="SID"){
							
							if ($alinea[0]==$tipo_proc){   // SID o STAR

								$name=$alinea[1];
								// if ($name!=$old && $listar){
								// 		$resu['procedures'][$old]=$proc[$old];
								// }
								
								$old=$name;

								$listar=true;

								$resu['procedure_list'][$name]=$name;
								$resu['procedures'][$name]['procfixs']=array();
								$resu['procedures'][$name]['runways']=array();
								$resu['procedures'][$name]['transitions']=array();
								$resu['procedures'][$name]['transition_list']=array();

								// primero el fijo de la pista de salida
								$contiene_rnw=array_search("RNW",$alinea);
								if ($contiene_rnw !== false){
									$rwy=$alinea[$contiene_rnw+1];
									$resu['procedures'][$name]['runways'][$rwy]=$this->getvalues($alinea,'FIX');
								}else{
									
									 $resu['procedures'][$name]['procfixs']=$this->getvalues($alinea,'FIX');
								}
							}else{
								// empieza con RNW
								if ($alinea[0]=="RNW"){
									//------------------------------------------
									// recorro el arreglo por si hay varias 
									// pistas en la misma linea
									//------------------------------------------
									for ($i=0;$i < count($alinea); $i++) {
										if ($alinea[$i]=="RNW"){
											$rwy=$alinea[$i+1];
											$resu['procedures'][$name]['runways'][$rwy]=$this->getvalues($alinea,'FIX');
										}
									}
									//------------------------------------------
								}

								if ($alinea[0]=="TRANSITION"){
									$transition_name=$alinea[1];
									$resu['procedures'][$name]['transitions'][$transition_name]=$this->getvalues($alinea,'FIX');
								}
							}  // fin: cambia de SID
						
				 } // if ($linea=="ENDSIDS"){					

				} // while (!feof($archi) and !$exit_stars){

			}  // if ($linea==$ini_proc){

		} // while todo el archivo
		fclose($archi);

		if ($listar){

				// ARMA PROCEDURE_LIST
				
				$temp=array();

				foreach ($resu['procedures'] as $key_proc => $value_proc) {
					foreach ($value_proc['runways'] as $key_rwy => $value_rwy) {
						if ($key_rwy==$runway){
							$temp[$key_proc]=$key_proc;
							break;
						}
					}
				}
				$resu['procedure_list']=$temp;

		}


		if (count($resu)>0 && count($resu['procedure_list'])>0){
			$temp['0']='seleccione...';
			$resu['procedure_list']=array_merge($temp,$resu['procedure_list']);
		}else{
			$resu['procedure_list']['']='sin '.($tipo_proc=="SID" ? "SID...": "STAR...");
		}

		$fixs=$this->get_airportdata($icao,false);
		$fixs=$fixs['fixs'];

		// array del PROCEDURE seleccionado 
		// para lista del combo de transiciones
		$resu['transition_list']=array();


		$line_final_app="";
		$linestring="";

		// var_dump($resu['procedures']);

		if ($proc_name!=false){
			if ($tipo_proc=="SID"){

				// if ($proc_name!=false){
				// 	$resu['transition_list']=array();
				// 	foreach ($resu['procedures'][$proc_name]['transitions'] as $key => $value) {
				// 		$resu['transition_list'][$key]=$key;
				// 	}
				// }

				// incluyo la pista de salida como FIJO
				$linestring.=",".$fixs["R.".$runway];

				foreach ($resu['procedures'][$proc_name]['runways'][$runway] as $key => $value) {
					$linestring.=",".$fixs[$key];
				}

				foreach ($resu['procedures'][$proc_name]['procfixs'] as $key => $value) {
					$linestring.=",".$fixs[$key];
				}

				if ($xtransition_name!=false){
					foreach ($resu['procedures'][$proc_name]['transitions'][$xtransition_name] as $key => $value) {
						$linestring.=",".$fixs[$key];
					}
				}
			}else{
				// ==== STAR ====
				
				$last_fix="";
				// 1. Primero transiciones				
				if ($xtransition_name!=false){
					foreach ($resu['procedures'][$proc_name]['transitions'][$xtransition_name] as $key => $value) {
						$linestring.=",".$fixs[$key];
						$last_fix=$fixs[$key];
					}
				}

				// 2. Luego Fijos del Procedimiento STAR
				foreach ($resu['procedures'][$proc_name]['procfixs'] as $key => $value) {
					$linestring.=",".$fixs[$key];
					$last_fix=$fixs[$key];
				}

				// 3. luego, los fijos del RNW
				foreach ($resu['procedures'][$proc_name]['runways'][$runway] as $key => $value) {
					$linestring.=",".$fixs[$key];
					$last_fix=$fixs[$key];
				}

				// // y por ultimo, el fijo de la pista 
				// $linestring.=",".$fixs["R.".$runway];

				$line_final_app.=$last_fix.",".$fixs["R.".$runway];

			}  // else STAR

			$resu['transition_list']=array();

			$temp=array();
			// armo lista para el COMBO transiciones
			if (count($resu['procedures'][$proc_name]['transitions'])>0){
				$temp["0"]="seleccione...";
				foreach ($resu['procedures'][$proc_name]['transitions'] as $key => $value) {
					$temp[$key]=$key;
				}
				
				$resu['transition_list']=$temp;
				
			}else
			{
				$resu['transition_list']['']="sin transiciones...";
			}

		}

		$linestring="LINESTRING(".substr($linestring, 1).")";
		$line_final_app="LINESTRING(".$line_final_app.")";

		$resu['route_final']=$line_final_app;
		$resu['route']=$linestring;	// well know text LINESTRING

		
		// var_dump($resu);
		return $resu;
	}

	function getvalues($array_linea,$clave){
		$result=array();
		$contiene_clave=array_search($clave, $array_linea);
		if ($contiene_clave!== false){	

			for ($i=0;$i+1 < count($array_linea);$i++){
				if ($array_linea[$i]==$clave){
					$skip=1;
					if ($array_linea[$i+$skip]=="OVERFLY"){
						$skip=2;
					}
					$result[$array_linea[$i+$skip]]=$array_linea[$i+$skip];
				}
			}
		}

		// retorna array con los FIX o lo que se busque en CLAVE:
		return $result;
	}


	//===========================================
	//  get APPROACHES LIST para el combo
	//===========================================
	function get_approaches_list($icao){

		$nomarchi="f:/fsx/pmdg/sidstars/".$icao.".txt";
		$archi=fopen($nomarchi,"r+");

		$salir=false;
		$app=array();

		while (!feof($archi) && !$salir){

			$linea=trim(fgets($archi));

			if ($linea=="APPROACHES"){
				while (!feof($archi) && !$salir){
					$linea=trim(fgets($archi));
					if ($linea=="ENDAPPROACHES"){
						$salir=true;
					}else{
						$alinea=explode(" ",$linea);
						if ($alinea[0]=="APPROACH"){
							$app[$alinea[1]]=$alinea[1];
						}
					}
				}	// while 2
			} // if

		} // while 1

		if (count($app)>0){
			$temp['']='seleccione...';
			$app=array_merge($temp,$app);
		}else{
			$app['']='sin approaches...';
		}

		return $app;

	}	


	//===========================================
	//  get APPROACH  para graficar ruta
	//===========================================
	function get_approach($icao, $approach,$xtransicion){

		$nomarchi="f:/fsx/pmdg/sidstars/".$icao.".txt";
		$archi=fopen($nomarchi,"r+");

		$salir=false;
		$app=array();
		$old="";
		$name="";
		$found=false;

		while (!feof($archi) && !$salir){

			$linea=trim(fgets($archi));

			if ($linea=="APPROACHES"){
				while (!feof($archi) && !$salir){
					$linea=trim(fgets($archi));
					if ($linea=="ENDAPPROACHES"){
						$salir=true;
					}else{
						$alinea=explode(" ",$linea);
						if ($alinea[0]=="APPROACH" && $alinea[1]==$approach){

								// APPROACHE ENCONTRADA
								$found=true;
								$final_fixs=array();
								$transitions_fixs=array();
								$transitions_list=array();

								while (!feof($archi) && !$salir){

									$name=$alinea[1];

									if ($alinea[0]=="APPROACH"){

										// hago lo siguiente para evitar que siga la ruta
										// por los fijos de la frustrada
										$pos=strpos ($linea , "RNW");
										$aux=substr($linea,0,$pos-1);
										$fijos=explode(" ",$aux);
										$final_fixs=$this->getvalues($fijos,"FIX");										
										
										$contiene_rnw=array_search("RNW",$alinea);
										if ($contiene_rnw!=false){
											
											$rwy="R.".$alinea[$contiene_rnw+1];

											$temp_rwy[$rwy]=$rwy;
											$final_fixs=array_merge($final_fixs,$temp_rwy);
										}

										$app['transitions_fixs']=array();
										$app['transitions_list']=array();

									}
									if ($alinea[0]=="TRANSITION"){
										$transition_name=$alinea[1];

										$transitions_list[$transition_name]=$transition_name;

										$transitions_fixs[$transition_name]=$this->getvalues($alinea,"FIX");
									}

									// leo otra linea y veo si salgo o genero approach
									$linea=trim(fgets($archi));
									$alinea=explode(" ",$linea);
									if ($alinea[0]=="APPROACH" || $alinea[0]=="ENDAPPROACHES"){
										$salir=true;

										$app['final_fixs']=$final_fixs;
										$app['transitions_fixs']=$transitions_fixs;
										$app['transitions_list']=$transitions_list;
									}
								} // while
						} // if linea[0]="approach" and alinea[1]==approach
					}
				}	// while 2
			} // if

		} // while 1


		fclose($archi);



		$data=$this->get_airportdata($icao);

		
		$linestring="";
		if ($found){

			if (count($app['transitions_list'])>0){
				$temp['0']='seleccione...';
				$app['transitions_list']=array_merge($temp,$app['transitions_list']);
			}else{				
				$app['transition_list']['0']='sin transiciones app';
			}


			// si viene parametro de transicion de app:
			if ($xtransicion!=""){
				if (count($app['transitions_fixs'])>0){
					foreach ($app['transitions_fixs'][$xtransicion] as $key => $value) {
						// $linestring.=",".$key;
						$linestring.=",".$data['fixs'][$key];
					}					
				}
			}

			// fijos del approach final
			foreach ($app['final_fixs'] as $key => $value) {
				// $linestring.=",[".$key."]";
				$linestring.=",".$data['fixs'][$key];
			}

			$linestring="LINESTRING(".substr($linestring,1).")";			

		}

		$app['route']=$linestring;

		return $app;

	}	


	function vectores_sid($icao,$pista, $procedimiento, $transicion){

		$archivo="F:/FSX/PMDG/SIDSTARS/".$icao.".TXT";
		$archi=fopen($archivo,'r+');

		// echo ((2000*100)/111000)."<br>";  // 2000 metros = 1.80 grados mas o menos
		$recta=array();
		$v=0;
		$vector_number=0;
		$vector=array();
		$last_pos="";
		$correccion=-90; // factor de correccion para llevar 0° a al norte en lugar de estar hacia el ESTE.
		$lineaSID="";
		$lineaRNW="";
		$lineaTRAN="";

		$salir=false;
		while (foef($archi) && !$salir){
			$linea=trim(fgets($archi));
			$alinea=explode(" ",$linea);

			if($alinea[0]=="SID" && $alinea[1]==$procedimiento){
				$rnw_pos=array_search("RNW",$alinea);
				if ($rnw_pos!=false){
					if ($alinea[$rnw_pos+1]==$pista){
						$lineaRNW=$linea;
						$vectores=$this->get_vectores($icao,$linea);
					}
				}

			}



	}

	 	 // var_dump($vector);


		$linea="";

		foreach ($vector as $key => $value) {
			$linea.= ",".$value;
		}

		$vectores="";
		foreach ($recta as $key => $value) {
			$vectores.="(".$value."),";
		}

		$vectores="";
		$linea="MULTILINESTRING(".$vectores."(".substr($linea, 1)."))";

		// var_dump($recta);
		// var_dump($linea);
		

		// $linea=",";
		// $linea.=$fixs['FDO'].",";
		// $linea.=$this->calcular_fix($fixs["FDO"],97,3);		
		// $linea="LINESTRING(".substr($linea, 1).")";

		// var_dump($vectors);
		// var_dump($linea);
		
		// $ruta['vector']="LINESTRING($x1 $y1, $x2 $y2)";



		$ruta['vector']=$linea;
		echo json_encode($ruta);

	} // function


	function get_vectores($icao,$linea){

		$data=$this->Model_procedures->get_airportdata($icao);
		// var_dump($data);
		$fixs=$data['fixs'];

		for ($i=0; $i<count($alinea);$i++){

		  if ($alinea[$i]=='RNW'){
			$rnw=$alinea[$i+1];
			$vector["R.".$rnw]=trim($fixs["R.".$rnw]);	// origen: la pista

			$hdg=$data['runway_info'][$rnw]['hdg']*1;

			// // las sig sentencias son para desplazarse al final 
			// // de la pista de partida (se convierte de pies a millas)
			// $length=$data['runway_info'][$rnw]['length']*1;
			// $length=($length*0.000164579)*0.016;   // PIES a MILLAS y millas a grados

			//========INVERTIR PISTA ==========
			$letra="";
				if (strlen($rnw)>2){
					$letra=substr($rnw,2,1);
					$rwy=substr($rnw,0,2)*10;  // agrego el cero a la derecha
				}else{
					$rwy=$rnw*10;  // agrego el cero a la 
				}
				if ($rwy<=180){
					$rwy=$rwy+180;
				}else
					$rwy=$rwy-180;
			// ya di vuela la pista
			$rwy=$rwy/10; // quito cero de la derecha
			if ($letra!=""){
				if ($letra=="L"){
					$letra="R";
				}
				if ($letra=="R"){
					$letra="L";
				}

				// solo para no olvidarme que esta la pista C (centro) 
				if ($letra=="C"){
					$letra="C";
				}
			}

			$lastpoint=$fixs["R.".$rnw];
			//=================================
			// echo "R.".$rnw."<br>";
			$lastpoint=$fixs["R.".$rwy.$letra];
			$vector_number++;
			$vector["vector_".$vector_number]=$fixs["R.".$rwy.$letra];

			$fixs["R.".$rwy.$letra];
			$lastpoint=$fixs["R.".$rwy.$letra];
			// echo "R.".$rnw." -> R.".$rwy.$letra."<br>";

			// $lastpoint=$this->calcular_fix($fixs["R.".$rwy.$letra],$hdg,$length);
		  } 

		  //========= TRK =====================		  

		  if ($alinea[$i]=="TRK" || $alinea[$i]=="HDG"){

		      $angulo=$alinea[$i+1]*1;
		      $angulo = $angulo;  // correcion para OSM (no se porque);

		      if ($alinea[$i+2]=="UNTIL"){
			      // si valor luego de TRK tiene punto "."
		          if (strpos($alinea[$i+3],".")){
		          	// hasta MILLAS desde ultimo FIX
		          	$distancia = ($alinea[$i+3]* 0.016);	// 0.016 => 1 milla o minuto de arco en grados  
		          }else{
		          	// hasta ALTITUD desde ultimo FIX
		          	$distancia = ($alinea[$i+3] / 100000)+0.02; // 0.005;	// 1nm
		          }
		          $p2=$this->calcular_fix($lastpoint,$angulo,$distancia);
		          // echo "$lastpoint , $angulo , $distancia <br>";

		          $vector_number++;
		          $vector["vector_".$vector_number]=$p2;
		          $lastpoint=$p2;
		      }
		   }

		   //========= HDG =====================

		   if ($alinea[$i]=="HDG"){
				$hdg=$alinea[$i+1]*1; // angulo
				// echo "HDG=> $hdg  ";
				if ($alinea[$i+2]=="INTERCEPT"){

					$radial=$alinea[$i+4]*1; // radial
					// echo "RADIAL=> $radial  ";
					$distancia=5; // 2 grados = 222 km 

					// PRIMER VECTOR HDG
					$p2_hdg=$this->calcular_fix($lastpoint,$hdg,$distancia);
					//$vector['last->'.$hdg]=$p2;
					$v++;
					$recta["v".$v]="$lastpoint,$p2_hdg"; // coord del vector

					// SEGUNDO VECTOR RADIAL FROM FIX
					$fijo=$fixs[$alinea[$i+7]];
					//$vector[$alinea[$i+7]]=$fijo;

					// $distancia=10;
					$p3=$this->calcular_fix($fijo,$radial,$distancia); 
					//$vector[$alinea[$i+7].'->'.$radial]=$p3;
					$v++;
					$recta["v".$v]="$fijo,$p3"; // coord del vector

					$interseccion=$this->intersection($lastpoint,$p2_hdg,$fijo,$p3);
					// echo $interseccion."<br>";
					$vector_number++;
					$vector['interseccion_'.$hdg.'->'.$radial]=$interseccion;
					$lastpoint=$interseccion;
				}
		   } // HDG

		   if ($alinea[$i]=="FIX" && $alinea[$i-1]!="TO" && $alinea[$i-1]!="FROM"){
		   		$vector_number++;
		   		$vector['vector_'.$vector_number]=$fixs[$alinea[$i+1]];
		   		$lastpoint=$fixs[$alinea[$i+1]];
		   }
		}// for		
	}


	function calcular_fix($p1,$xang,$distancia){
		$coord=explode(" ",trim($p1));
		$x1=$coord[0]*1;
		$y1=$coord[1]*1;

		// si es TRK entonces DISTANCIA=negativa;
		// si es HDG entonces DISTANCIA=positiva;
		// $ang=97;
		$correccion=-90;
		// $DistanciaR13=10;
        $angulo= ($xang) + $correccion;
        $angradian=(-$angulo) * (M_PI/180);
	    $x2=(cos($angradian)*$distancia)+$x1;
	    $y2=(sin($angradian)*$distancia)+$y1;

// 	    echo "<br>>>>>>> x2= $x2  y2= $y2<br>";
	    return "$x2 $y2";

	}

	function intersection ($p1,$p2,$p3,$p4 ) {
		$p1=explode(" ",trim($p1));
		$p2=explode(" ",trim($p2));
		$p3=explode(" ",trim($p3));
		$p4=explode(" ",trim($p4));

		$ax1 = $p1[0];
		$ay1 = $p1[1];
		$ax2 = $p2[0];
		$ay2 = $p2[1];
		$bx1 = $p3[0];
		$by1 = $p3[1];
		$bx2 = $p4[0];
		$by2 = $p4[1];


	    $a1 = $ax1 - $ax2;
	    $a2 = $bx1 - $bx2;

	    $b1 = $ay1 - $ay2;
	    $b2 = $by1 - $by2;

	    $c = ( $a1 * $b2 ) - ( $b1 * $a2 );

	    if ( abs ( $c ) > 0.01 ) { // En caso de que haya interseccion

	        $a = ( $ax1 * $ay2 ) - ( $ay1 * $ax2 );
	        $b = ( $bx1 * $by2 ) - ( $by1 * $bx2 );

	        $x = ( $a * $a2 - $b * $a1 ) / $c;
	        $y = ( $a * $b2 - $b * $b1 ) / $c;

	    } else { // En caso de que no lo haya

	        $x = "";
	        $y = "";

	    }

	    return "$x $y";

	 }


 function gettour(){
      $archivo="F:/FSX/PMDG/NAVDATA/wpNavAPT.txt";
      $archi=fopen($archivo,"r+");

      $icao['PASC']='PASC';
      $icao['PAFA']='PAFA';
      $icao['CYXY']='CYXY';
      $icao['CYEG']='CYEG';
      $icao['KMSP']='KMSP';
      $icao['KDEN']='KDEN';
      $icao['KRND']='KRND';
      $icao['MMMX']='MMMX';
      $icao['MSLP']='MSLP';
      $icao['MPTO']='MPTO';
      $icao['SKCL']='SKCL';
      $icao['SEQM']='SEQM';
      $icao['SPJC']='SPJC';
      $icao['SCFA']='SCFA';
      $icao['SCEL']='SCEL';
      $icao['SAEZ']='SAEZ';
      $icao['SAVC']='SAVC';

      $salir=false;

      while (!feof($archi) && !$salir){
          $linea=fgets($archi);
          $aip=substr($linea,24,4);
          if (array_search($aip, $icao)!==false){
              $lon=trim(substr($linea,49,11));
              $lat=trim(substr($linea,39,10));
              $icao[$aip]="$lon $lat";
          }
      }
      fclose($archi);

      // var_dump($icao);

      $linea="";
      foreach ($icao as $key => $value) {
      	$linea.=",".$value;
      }

      if ($linea !=""){
      	$linea="LINESTRING(".substr($linea, 1).")";
      }else{
      	$linea="LINESTRING empty";
      }

      // var_dump($linea);

      return $linea;

}	


}



