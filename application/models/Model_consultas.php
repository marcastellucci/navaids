<?php 

Class Model_consultas extends CI_Model{

function get_infototal($icao,$tipo_proc){

    $info=array();

    $rnw=array();
    $transitions=array();
    $fix1=array();
    $fix2=array();  


    // abro archivo  SIDSTARS: del ICAO solicitado:
    $xarchi=fopen("F:/fsx/pmdg/sidstars/".$icao.".txt","r");

    while (!feof($xarchi) ){
        $cad=trim(fgets($xarchi ));
        $cad=explode(" ",$cad);

        $salir_procedure=false;
        $salir_star=false;
        $salir_fix=false;        

        //============ COMIENZA FIXS ========================
        if ($cad[0]==="FIXES"){
            $cad=explode(" ",trim(fgets($xarchi)));   
            while (!feof($xarchi) && !$salir_fix){            
                
                $fix_name=$cad[1];
                $lon="";
                $lat="";
                if ($cad[0]==="FIX"){

                    if ($cad[3]==="S")
                        $lat="-";
                    if ($cad[6]==="W")
                        $lon="-";

                    $lat.=$cad[4]+substr(($cad[5]/60),1);
                    $lon.=$cad[7]+substr(($cad[8]/60),1);
                    $info['fixes'][$cad[1]] = "$lon $lat";                    
                }


                $cad=explode(" ",trim(fgets($xarchi))); 
                if ($cad[0]==="ENDFIXES"){
                    $salir_fix=true;
                }                  
            }// while                
        }

        //============== COMIENZA SIDS ==========================
        $proc=-1;
        $iTransi=-1;
        $proc_name="";
        $transition_name="";        
        // $procedure."S"  =>  SIDS o STARS
        if ($cad[0]===$tipo_proc."S"){
            $linea=trim(fgets($xarchi));
            // echo "linea antes while = ".$linea."<br>";
            $cad=explode(" ",$linea); 
            //---------------------
            // WHILE 
            //---------------------
            while (!feof($xarchi) && !$salir_procedure){

                //------------------------------------------
                // si cadena empieza con SID
                //------------------------------------------
                if ($cad[0]=== $tipo_proc){

                    //=========================================================
                    // si no es la primer SID, grabo datos
                    //=========================================================

                    if ($proc >= 0){  
                        
                        // $info[$sid]['sidname']=$sidname;
                        // echo "sid=$sid -- info['sid'][$sidname]<br>";
                        $info['procedimiento'][$proc_name]['rnw']=$rnw;
                        $info['procedimiento'][$proc_name]['transitions']=$transitions;
                        $info['procedimiento'][$proc_name]['fix1']=$fix1;
                        $info['procedimiento'][$proc_name]['fix2']=$fix2;
                    }
                    //=========================================================
                    
                    $proc++;
                    // echo $sid.' / '.$sidname."<br>";
                    $rnw=array();
                    $transitions=array();
                    $fix1=array();
                    $fix2=array();

                    $proc_name=$cad[1]; 
                    // echo "sidname=$sidname  (cad[1])<br>";
                    $transition_name="";
                    $contiene_rnw=array_search("RNW", $cad);
                    
                    $runway="";
                    // obtengo RNW y FIX en la linea que empieza con SID:
                    for ($i=0;$i<count($cad);$i++){
                        
                        if ($cad[$i]=="RNW"){
                            $runway=$cad[$i+1];
                            $rnw[]=$runway;
                            $fix1[$runway][]=$runway;
                        }
                        // MISMA CADENA DE empieza con SID:
                        if ($cad[$i]=="FIX"){
                            $skip=1;
                            if ($cad[$i+1]=="OVERFLY"){
                                $skip=2;
                            }
                            if ($contiene_rnw!==false){
                                $fix1[$runway][]=$cad[$i+$skip];                                
                            }else{            
                                // estos fijos son comunes a todas las pistas
                                // por eso no llevan indice clave con nro pista
                                // como si lo hacen los FIX1..                    
                                $fix2[]=$cad[$i+$skip];
                            }

                        }
                    }//for
                }

                //------------------------------------------
                // SINO: si empieza con RNW
                //------------------------------------------
                if ($cad[0]==="RNW"){                    
                    $runway=$cad[1];
                    $rnw[]=$runway;   
                    $fix1[$runway][]=$runway;                 
                    // si contiene FIXS los apilo
                    for ($i=0;$i+1<count($cad);$i++){
                        if ($cad[$i]==="FIX"){
                            // indice es la pista:
                            // ejemplo:  $fix1['09L']
                            $skip=1;
                            if ($i+$skip<=count($cad) && $cad[$i+1]=="OVERFLY"){
                                $skip=2;
                            }                            
                            $fix1[$runway][]=$cad[$i+$skip];
                        }
                    }                    
                }
                //------------------------------------------
                // SINO: si empieza con TRANSITION
                //------------------------------------------
                if ($cad[0]==="TRANSITION"){
                    $transition_name=$cad[1];                    

                    $transi_fixs=array();
                    for ($i=0;$i+1<count($cad);$i++){
                        $skip=1;
                        if ($i+$skip<=count($cad) && $cad[$i+1]=="OVERFLY"){
                            $skip=2;
                        }                                
                        if ($cad[$i]==="FIX"){
                            $transi_fixs[]=$cad[$i+$skip];
                        }
                    }
                    $transitions[$transition_name]=$transi_fixs;
                }
                // leo proxima linea del archivo
                $linea = trim(fgets($xarchi));
                // echo "linea = ".$linea."<br>";
                $cad=explode(" ",$linea);   
                // ENDSIDS o ENDSTARS
                if ($cad[0]=="END".$tipo_proc."S"){
                    $salir_procedure=true;
                }
            }// while
            //=================================================
            // GRABO ultima SID que no se grabo del bucle
            //=================================================
            $info['procedimiento'][$proc_name]['rnw']=$rnw;
            $info['procedimiento'][$proc_name]['transitions']=$transitions;
            $info['procedimiento'][$proc_name]['fix1']=$fix1;
            $info['procedimiento'][$proc_name]['fix2']=$fix2;
            //=================================================
   
        }//if        
    }// while


    fclose($xarchi);

    return $info;
}


function get_airportdata($icao){
    
    $resu=array();
    $centro=array();
    $xarchi=fopen("F:/FSX/PMDG/NAVDATA/wpNavAPT.txt","r+");
    $salir=false;
    $airport_name="";
    while (!feof($xarchi) && !$salir){
        $linea=trim(fgets($xarchi));
        if (substr($linea,24,4)===$icao){
            $centro['lon']=substr($linea,50,10)*1;
            $centro['lat']=substr($linea,39,10)*1;

            $runway=trim(substr($linea,28,3));
            // obtengo coord de la pista encontrada
            $resu['runway'][$runway]=substr($linea,50,10)." ".substr($linea,39,10);
            $airport_name=substr($linea,0,24);
        }
    }
    fclose($xarchi);

    // OJO: agrego al array RESU el array con el CENTRO 
    $resu['centro']=$centro;
    $resu['airport_name']=$airport_name;

    return $resu;

}

function get_route($icao,$sid_o_star,$pista,$sid_name,$transition){

    $route="vacio";

    $resu=$this->proceso_navaids($icao);
    // if (count($resu)>0){
    //     for ($i=0;$i<count($resu);$i++){
    //         if ($resu[$i]['runway']==$pista){
    //             if ($resu[$i]['sid'])
    //         }            
    //     }
    // }
    var_dump($resu);

    return $route;

}

function buscar_pistas($icao){
    global $archi;
    global $info;

        $runways=array();

        $runways['fixes'][]=$this->get_fixes($icao);  // geojson string
        $runways['centro'][]=$this->get_center($icao);

        $resu=$this->proceso_navaids($icao);
        // var_dump($resu); die();

        $runways['pistas'][]='Seleccione...'; // vacio para que se deba elegir si o si una pista y actue el evento CHANGE
        if (count($resu)>0){
            for ($i=0;$i<count($resu);$i++){
                $pos=array_search($resu[$i]['runway'],$runways['pistas']);                
                if ($pos===false){
                        $runways['pistas'][]=$resu[$i]['runway'];
                }
            }

        }else{
            $runways="vacio";
        }

        return $runways;
    }


function get_center($icao){
    $centro=array();

    $xarchi=fopen("F:/FSX/PMDG/NAVDATA/wpNavAPT.txt","r+");
    $salir=false;
    while (!feof($xarchi) && !$salir){
        $linea=trim(fgets($xarchi));
        if (substr($linea,24,4)===$icao){
            $centro['lon']=substr($linea,50,10)*1;
            $centro['lat']=substr($linea,39,10)*1;
            $salir=true;
        }
    }
    fclose($xarchi);

    return $centro;

}

function buscar_procedimiento($icao,$sid_star,$pista){   
    global $info;

        $resu=array();


        $xinfo=$this->proceso_navaids($icao); 

        $resu[]='Seleccione...'; // vacio para que se deba elegir si o si una pista y actue el evento CHANGE
        if (count($xinfo)>0){
            // para cada pista:
            for ($i=0;$i<count($xinfo);$i++){
                // si la pista es la del parametro:
                if ($xinfo[$i]['runway']===$pista){ 
                    // si hay cargada una SID en el array:
                    if ($sid_star === "SID"){
                        if ($xinfo[$i]['sid']!==''){
                            // me fijo si la SID ya esta cargada en el array de salida:               
                            $pos=array_search($xinfo[$i]['sid'],$resu);                
                            // si no esta, la cargo al array:
                            if ($pos===false){
                                    $resu[]=$xinfo[$i]['sid'];
                            }
                        }
                    else{
                        if ($xinfo[$i]['star']!==''){
                            // me fijo si la SID ya esta cargada en el array de salida:               
                            $pos=array_search($xinfo[$i]['star'],$resu);                
                            // si no esta, la cargo al array:
                            if ($pos===false){
                                    $resu[]=$xinfo[$i]['star'];
                            }
                        }

                    }

                }    
                    }
            }

        }else{
            $resu="vacio";
        }


        // $resu[]="13L";
        // $resu[]="13R";
        // $resu[]="31L";
        // $resu[]="31L";
        // $resu[]="23L";
        // $resu[]="23R";

        return $resu;
    }

function get_transiciones($icao, $sid_o_star, $sidstar_name, $pista){   
    global $info;

        $resu=array();

        $xarchi=fopen('log.txt','w');

        $xinfo=$this->proceso_navaids($icao); 

        $resu[]='Seleccione...'; // vacio para que se deba elegir si o si una pista y actue el evento CHANGE
        if (count($xinfo)>0){
            // para cada pista:
            for ($i=0;$i<count($xinfo);$i++){
                // si la pista es la del parametro:
                if ($xinfo[$i]['runway']===$pista){ 
                    // si hay cargada una SID en el array:
                    // if ($xinfo[$i]['sid']!==''){
                    //     // me fijo si la SID ya esta cargada en el array de salida:               
                    //     $pos=array_search($xinfo[$i]['sid'],$resu);                
                    //     // si no esta, la cargo al array:
                    //     if ($pos===false){
                    //             $resu[]=$xinfo[$i]['sid'];
                    //             fputs($xarchi,$xinfo[$i]['sid']);
                    //     }
                    // }
                    if ($xinfo[$i]['sid']===$sidstar_name){
                        if ($xinfo[$i]['transicion']!== ""){
                            $resu[]=$xinfo[$i]['transicion'];
                        }
                    }
                }    
            }

        }else{
            $resu="vacio";
            fputs($xarchi,$resu);
        }

        fclose($xarchi);

        // $resu[]="13L";
        // $resu[]="13R";
        // $resu[]="31L";
        // $resu[]="31L";
        // $resu[]="23L";
        // $resu[]="23R";

        return $resu;
    }

    function get_airports(){

        global $archi;

        $archi=fopen("F:/FSX/PMDG/NAVDATA/wpNavAPT.txt","r+");
        $old_icao="";
        $airport_name="";
        $airport_icao="";
        $airports="<select class='form-control' id='selector_aip' name='selector_aip'><option value=''></option>";      

        $aips=array();
        $i=0;

        while (!feof($archi)){
            $linea=fgets($archi);        
            if (substr($linea,0,1)!=";"){
                $airport_icao=substr($linea,24,4);
                if ($airport_icao !== $old_icao){
                    $old_icao=$airport_icao;
                    $airport_name=substr($linea,0,23);

                    $aips[]="$airport_icao|$airport_name";  

                }
            }
            
        }

        asort($aips);

        foreach ($aips as $aip => $aerop) {
            $explo=explode("|",$aips[$aip]);            
            $airports=$airports."<option value='".$explo[0]."'>".$explo[0]." - ".$explo[1]."</option>"; 
        }


        $airports=$airports."</select>";
        fclose($archi);

        return $airports;
    }


 //======================PROCESA NAVAIDS=============================================
 function proceso_navaids($icao_buscado){
    global $archi;
    global $info;
    global $indice;
    global $afixes;

    $info=array();
    // PARAMETRO 
    $airport=$icao_buscado;

    $en_fixes = 1;
    $rnw_procesado = 0;
    $en_sids = 3;
    $en_stars=4;
    $actual=0;
    $afixes=array();
    $aSIDS=array();
    $aRNWS=array();
    $aTransitions=array();

    $info=array();
    $indice=0;

    $archi=fopen("F:/FSX/PMDG/NAVDATA/wpNavAPT.txt","r+");
    $salir=false;
    $aeropuerto="";
    while (!feof($archi) && !$salir){
        $linea=fgets($archi);        
        if (substr($linea,24,4)==$airport){            
            $aeropuerto=substr($linea,0,23);
            while (!feof($archi) && substr($linea,24,4)==$airport){
                $aRNWS["R.".trim(substr($linea,28,3))]=substr($linea,50,10)." ".substr($linea,39,10);
                $afixes["R.".trim(substr($linea,28,3))]=substr($linea,50,10)." ".substr($linea,39,10);
                $linea=fgets($archi);
            }
            $salir=true;
        }
    }
    fclose($archi);


    if ($aeropuerto == ""){
        echo "#########################################<br>";
        echo "   ERROR:   No se encontró el ICAO <br>";
        echo "#########################################<br>";
        die();
    }

    // echo "============================<br>$aeropuerto<BR>============================<br>";
    

    $archivo_icao="F:/FSX/PMDG/SIDSTARS/".$airport.".txt";
    if (!file_exists($archivo_icao)){
        echo "#########################################<br>";
        echo "   ERROR:  No se encontró el ARCHIVO <br>";
        echo "#########################################<br>";
        die();
    }



    $archi=fopen($archivo_icao,"r+");
    while(!feof($archi)) {

        $linea = ltrim(fgets($archi));  
       
        if (substr($linea,0,1)!=="/"){
            $a=explode(" ",$linea);
            
            switch (trim($a[0])) {
                case 'FIXES':   
                    $this->procesa_fix($a); 
                    break;
                case "RNWS":                                                        
                    $this->procesa_rnws();
                    break;
                case 'SIDS':
                    $this->procesa_sid();
                    break;                
                case 'STARS':
                    // $this->procesa_star();
                    break;                

            }

        }
    }
    fclose($archi);


    // echo "</br>---------------------------------------------</br>";
    // echo "<br>================================<br>";
    // $sid="LINESTRING(".$afixes['JUA'].",".$afixes['BITAG'].",".$afixes['AF001'].",".$afixes['AF002'].",".
    //       $afixes['D034N'].",".$afixes['AF003'].",".$afixes['AF004'].",".$afixes['D058N'].",".$afixes['AF005'].",".$afixes['AF006'].",".$afixes['D088N'].",".$afixes['KIBUN'].")";

    // echo "<BR>".$sid;
    // echo "<br>";
    // print_r($aSIDS);


    //=========== CONVIERTO LOS FIJOS EN COORDENADAS PARA LINESTRING ============
    
    
    if (count($info)>0){
        // para cada SID almacenada en INFO:
        for ($i=0;$i<count($info);$i++){
            // si la SID actual tiene FIJOS para LINESTRING
            $wkt="";
            if (count($info[$i]['linestring'])>0){
                // para cada SID reemplazar cada nombre de FIJO 
                // por la coordenada almacenada en el array asoc $aFIXES
                // ejemplo:  $info[$i]['linestring'][$j]=$info[$i]['linestring']['ESITO']
                $wkt="LineString(";
                for ($j=0;$j<count($info[$i]['linestring']);$j++){
                    // $info[$i]['linestring'][$j]=$afixes[$info[$i]['linestring'][$j]];
                    $info[$i]['linestring'][$j] =$afixes[$info[$i]['linestring'][$j]];
                    //====================================                    
                    //       WKT  WELLKNOWTEXT
                    //====================================
                     $wkt = $wkt.$info[$i]['linestring'][$j].",";       
                    //====================================                    
                }
                $wkt=substr($wkt, 0, strlen($wkt)-1).")";
            }else
            {
                $wkt="";
            }
            $info[$i]['linestring']=$wkt;
        }
    }
    //===========================================================================
    
    return $info;

 
 }
// ---------------------------  FIN MAIN PROGRAM -------------------


function procesa_rnws(){
    global $aRNWS;
    global $archi;
    global $rnw_procesado;

    $salir=false;
    // if ($rnw_procesado==1){
    //     $salir=true;
    // }else{
    //     $rnw_procesado=1;
    // }
    while (!feof($archi) and !$salir){
        $linea=trim(fgets($archi));
        $arre=explode(" ",$linea);        
        if ($arre[0]=="RNW"){
            $aRNWS[]="R.".$arre[1];
        }
        if ($arre[0]=="ENDRNWS"){
            $salir=true;
        }
    }
     
}

function procesa_fix(){
    global $afixes;
    global $archi;
    global $info;
    global $indice;
    global $archi;

          
    $salir=false;
    $linea=trim(fgets($archi));
    $arre_linea=explode(" ",$linea);
    $multipoint="";
    $multipoint_GeoJSON="";
    $sql="";
    $lon="";$lat="";
    
    while (!feof($archi) and $arre_linea[0]!=="ENDFIXES"){        
        
        if (count($arre_linea)>1){
                $fix_name=$arre_linea[1];
                $lon="";
                $lat="";
                if ($arre_linea[0]=="FIX"){
                    if ($arre_linea[3]=="S")
                        $lat="-";
                    if ($arre_linea[6]=="W")
                        $lon="-";

                    $lat.=$arre_linea[4]+substr(($arre_linea[5]/60),1);
                    $lon.=$arre_linea[7]+substr(($arre_linea[8]/60),1);
                    $afixes[$arre_linea[1]] = "$lon $lat";
                    //====================================
                    $multipoint_GeoJSON=$multipoint_GeoJSON."{ 'type': 'Feature',<br> 'geometry': {'type': 'Point', 'coordinates': [$lon, $lat]},<br> 'properties': {'fixname': '$fix_name'}<br>},";            
                    $multipoint=$multipoint."($lon $lat),";
                    //====================================                    
                    $sql=$sql."insert into fijos(fixname,lon,lat,lonlat,the_geom)values(
                                '$fix_name','$lon','$lat','$lon $lat',ST_GeomFromText('Point($lon $lat)'));<br>";
                    //====================================                    
                }
        }
        $linea=trim(fgets($archi));        
        $arre_linea=explode(" ",$linea);


    }   // fin while


    // // echo "<br>============<br>$sql<br>====================<br>";

    //  echo "<br>============<br>$sql<br>====================<br>";
    // echo " GeoJSON>>>><br> { 'type': 'FeatureCollection',<br>'features': [".substr($multipoint_GeoJSON,0,strlen($multipoint_GeoJSON)-1)."]} <br> <<<<<<<< <br><br>";
    //  echo " WellKnowText>>>>><br> MULTIPOINT(".substr($multipoint,0,strlen($multipoint)-1).") <br> <<<<<<< <br>";


    
}


function get_fixes($icao){
 
    $archivo='f:/fsx/pmdg/sidstars/'.$icao.'.txt';
    if (!file_exists($archivo)){
        $geojson = "vacio";
        return $geojson;
    }

    $archi=fopen($archivo,'r');

    $salir=false;
    $linea=trim(fgets($archi));
    $arre_linea=explode(" ",$linea);
    $multipoint="";
    $multipoint_GeoJSON="";
    $sql="";
    $lon="";$lat="";
    
   $geojson = array(
        'type'      => 'FeatureCollection',
        'features'  => array()
    );
    $id=0;

   

    while (!feof($archi) and $arre_linea[0]!=="ENDFIXES"){        
        
        if (count($arre_linea)>1){
                $fix_name=$arre_linea[1];
                $lon="";
                $lat="";
                if ($arre_linea[0]=="FIX"){
                    if ($arre_linea[3]=="S")
                        $lat="-";
                    if ($arre_linea[6]=="W")
                        $lon="-";

                    $lat.=$arre_linea[4]+substr(($arre_linea[5]/60),1);
                    $lon.=$arre_linea[7]+substr(($arre_linea[8]/60),1);
                    $afixes[$arre_linea[1]] = "$lon $lat";

                    //====================================                    
                    //       G E O J S O N
                    //====================================

                    $feature = array(
                            'id' => $id,
                            'type' => 'Feature', 
                            'geometry' => array(
                                'type' => 'Point',
                                # Pass Longitude and Latitude Columns here
                                // multiplico x 1 pq sino la coordenada 
                                // se genera como string, en cambio
                                // asi, se genera como "numerico"
                                'coordinates' => array($lon*1,$lat*1)
                            ),
                            # Pass other attribute columns here
                            'properties' => array(
                                'fixname' => $fix_name
                                )
                            );
                        # Add feature arrays to feature collection array
                        array_push($geojson['features'], $feature);                                
                    //====================================

                }
        }
        $linea=trim(fgets($archi));        
        $arre_linea=explode(" ",$linea);

        $id++;

    }   // fin while

    fclose($archi);

    return $geojson;
}


function procesa_sid(){
    global $archi;
    global $afixes;
    global $aRNWS;
    global $info;
    global $indice;

    $pistas = array();
    $transiciones=array();
    $fijos=array();
    $fijos_fin=array();
    $cont_sids=0;
    $cont_trans=-1;
    $sid_name = "";

    // leer linea y explotarla
    $linea=trim(fgets($archi));
    $arre=explode(' ',$linea);
    $indice=0;

    $path_coords=array();


    while (!feof($archi) && $arre[0] !== 'ENDSIDS'){    

        // si empieza con SID:
        if ($arre[0]=="SID"){       

            // si no es primera SID, entonces GUARDAR SID y reiniciar todo
            if ($cont_sids>0){

                      for ($p=0;$p<count($pistas);$p++){
                        $sid=$sid_name." / R.".$pistas[$p];
                        $path_coords[] = "R.".$pistas[$p];
                        //======================================================
                        // arreglo que contiene toda la info 
                        // para llenar los controles de la vista
                        $info[$indice]['runway']=$pistas[$p];
                        $info[$indice]['sid']=$sid_name;                        
                        //======================================================

                        if (count($transiciones)>0){
                            // con TRANSICIONES
                            for ($t=0;$t<count($transiciones);$t++){

                                $sid=$sid_name." / R.".$pistas[$p]." / T.".$transiciones[$t][0];  
                               
                                
                                //======================================================
                                $info[$indice]['runway']=$pistas[$p];
                                $info[$indice]['sid']=$sid_name;                                  
                                $info[$indice]['transicion']=$transiciones[$t][0];
                                //======================================================

                                if (count($fijos[$pistas[$p]])>0){
                                    for ($f=0;$f<count($fijos[$pistas[$p]]);$f++){
                                        $sid=$sid." > ".$fijos[$pistas[$p]][$f];

                                        // guarda fijo con coordenada para el LINESTRING
                                        $path_coords[] = $fijos[$pistas[$p]][$f];
                                    }// for
                                } // if


                                if (count($fijos_fin)>0){
                                    for ($ff=0;$ff<count($fijos_fin);$ff++){
                                        $sid=$sid." >> ".$fijos_fin[$ff];
                                        // guarda fijo con coordenada para el LINESTRING
                                        $path_coords[] = $fijos_fin[$ff];

                                    } // for
                                } // if
                                if (count($transiciones[$t])>0){    
                                    for ($f=1;$f<count($transiciones[$t]);$f++){
                                        $sid=$sid." | ".$transiciones[$t][$f];
                                        // guarda fijos de las transiciones con coordenada para el LINESTRING
                                        $path_coords[] = $transiciones[$t][$f];

                                    } // for
                                } //if                  

                                $info[$indice]['linestring']=$path_coords;
                                $path_coords=array();
                             //================================================
                             // Indice para ARREGLO DE INFO
                              $indice++;
                              //================================================

                                // echo "<BR> #### ".$sid."<BR>";
                            }
                        }else{
                             // sin TRANSICIONES
                              // var_dump($fijos);

                            $info[$indice]['transicion']="";

                              if (count($fijos)>0){
                                  for ($f=0;$f<count($fijos[$pistas[$p]]);$f++){
                                        $sid=$sid." -> ".$fijos[$pistas[$p]][$f];
                                        $path_coords[]=$fijos[$pistas[$p]][$f];
                                  }
                              }
                            if (count($fijos_fin)>0){
                                for ($ff=0;$ff<count($fijos_fin);$ff++){
                                    $sid=$sid." => ".$fijos_fin[$ff];
                                    $path_coords[]=$fijos_fin[$ff];
                                }
                            }                          
                            // echo "<BR> **** ".$sid."<BR>";
                            $info[$indice]['linestring']=$path_coords;
                            $path_coords=array();
                             //================================================
                             // Indice para ARREGLO DE INFO
                              $indice++;
                              //================================================

                        }
                    } // for pistas

                    // echo "<br>#####################################################<br>";
                    // var_dump($aRNWS);
                    // var_dump($pistas);
                    // var_dump($fijos);
                    // echo "<br>#####################################################<br>";

                    $pistas=array();                
                    $transiciones=array();
                    $sid_name="";
                    $fijos=array();
                    $fijos_fin=array();
                    $cont_trans=-1;

            }// si cont_sids>0

            // guardo nombre de SID
            $sid_name = $arre[1];  // nombre del SID

            // y si contiene RNW
            $pos=array_search("RNW",$arre);
            if ($pos !== false){
                $pistas[]=$arre[$pos+1];


                // guardo todos los FIJOS INICIALES
                for ($i=0;$i<count($arre);$i++){
                    if ($arre[$i]=="FIX"){
                        $index=1;
                        if ($arre[$i+1]=="OVERFLY"){
                            $index=2;
                        }
                        $fijos[$arre[$pos+1]][]=$arre[$i+$index];
                        // $fijos[$arre[$pos+1]][]=$afixes[$arre[$i+$index]];
                    } // if
                } // for
            } // si NO tiene RNW
            else{
                // guardo todos los FIJOS FINALES
                for ($i=0;$i<count($arre);$i++){
                    if ($arre[$i]=="FIX"){
                        $index=1;
                        if ($arre[$i+1]=="OVERFLY"){
                            $index=2;
                        }
                        $fijos_fin[]=$arre[$i+$index];
                        // $fijos_fin[]=$afixes[$arre[$i+$index]];
                    } //if
                } // for                
            } // else

            $cont_sids++;
        } // si NO empieza con SID
        else{
            // si empieza con TRANSTIION
            if ($arre[0]=="TRANSITION"){

                $cont_trans++;
                $transit_name=$arre[1];

                // primer elemento es el nombre de TRANSICION
                $transiciones[$cont_trans][]=$transit_name;
                // guardo todos los FIJOS de la TRANSITION
                for ($i=0;$i<count($arre);$i++){
                    if ($arre[$i]=="FIX"){
                        // $transiciones[$cont_trans][]=$afixes[$arre[$i+1]];
                        $transiciones[$cont_trans][]=$arre[$i+1];
                    } // if                
                } // for
            } // si NO empieza con TRANSITION
            else{
                // si empieza con RNW
                if ($arre[0]=="RNW"){
                    $pistas[]=$arre[1];

                    // guardo todos los FIJOS de la SID que empieza con RNW
                    for ($i=0;$i<count($arre);$i++){
                        if ($arre[$i]=="FIX"){
                           // $fijos[$arre[1]][]=$afixes[$arre[$i+1]];
                            $fijos[$arre[1]][]=$arre[$i+1];
                        } // if                
                    } // for
                } // fin si NO empieza con TRANSITION y SI empieza con RNW
            }// fin si NO empieza con TRANSITION
        }
        // leer linea y explotarla
        $linea=trim(fgets($archi));
        $arre=explode(' ',$linea);

    }   // while

                   for ($p=0;$p<count($pistas);$p++){
                        $sid=$sid_name." / R.".$pistas[$p];

                        $path_coords[] = "R.".$pistas[$p];

                        //======================================================
                        // arreglo que contiene toda la info 
                        // para llenar los controles de la vista
                        $info[$indice]['runway']=$pistas[$p];
                        $info[$indice]['sid']=$sid_name;                       

                        //======================================================

                        if (count($transiciones)>0){
                            // con TRANSICIONES
                            for ($t=0;$t<count($transiciones);$t++){

                                $sid=$sid_name." / R.".$pistas[$p]." / T.".$transiciones[$t][0];
                                

                                //======================================================
                                $info[$indice]['runway']=$pistas[$p];
                                $info[$indice]['sid']=$sid_name;                                 
                                $info[$indice]['transicion']=$transiciones[$t][0];
                                
                                //======================================================


                                if (count($fijos[$pistas[$p]])>0){
                                    for ($f=0;$f<count($fijos[$pistas[$p]]);$f++){
                                        $sid=$sid." > ".$fijos[$pistas[$p]][$f];

                                        $path_coords[]=$fijos[$pistas[$p]][$f];
                                    }
                                }


                                if (count($fijos_fin)>0){
                                    for ($ff=0;$ff<count($fijos_fin);$ff++){
                                        $sid=$sid." >> ".$fijos_fin[$ff];
                                        $path_coords[] = $fijos_fin[$ff];
                                    }
                                }
                                if (count($transiciones[$t])>0){    
                                    for ($f=1;$f<count($transiciones[$t]);$f++){
                                        $sid=$sid." | ".$transiciones[$t][$f];

                                        $path_coords[] = $transiciones[$t][$f];
                                    }
                                }                            
                                // echo "<BR> #### ".$sid."<BR>";

                                $info[$indice]['linestring'] = $path_coords;
                                $path_coords=array();
                                $indice++;
                            }
                        }else{
                             // sin TRANSICIONES
                            $info[$indice]['transicion']="";

                              for ($f=0;$f<count($fijos[$pistas[$p]]);$f++){
                                    $sid=$sid." -> ".$fijos[$pistas[$p]][$f];
                                    
                                    $path_coords[] = $fijos[$pistas[$p]][$f];

                              }
                                if (count($fijos_fin)>0){
                                    for ($ff=0;$ff<count($fijos_fin);$ff++){
                                        $sid=$sid." => ".$fijos_fin[$ff];

                                        $path_coords[] = $fijos_fin[$ff];
                                    }
                                }       
                                $info[$indice]['linestring'] = $path_coords;
                                $path_coords=array();
                             //================================================
                             // Indice para ARREGLO DE INFO
                              $indice++;
                              //================================================

                                // echo "<BR> **** ".$sid."<BR>";
                        }
                    } // for pistas


    }   // function procesa_sid_nuevo


function procesa_star(){
    global $archi;
    global $afixes;
    global $aRNWS;
    global $info;
    global $indice;

    $pistas = array();
    $transiciones=array();
    $fijos=array();
    $fijos_fin=array();
    $cont_sids=0;
    $cont_trans=-1;
    $sid_name = "";

    // leer linea y explotarla
    $linea=trim(fgets($archi));
    $arre=explode(' ',$linea);

    $info=array();
    $indice=0;


    while (!feof($archi) && $arre[0] !== 'ENDSTARS'){

        // si empieza con STAR:
        if ($arre[0]=="STAR"){

            // si no es primera SID, entonces GUARDAR SID y reiniciar todo
            if ($cont_sids>0){

                      for ($p=0;$p<count($pistas);$p++){
                        $sid=$sid_name." / R.".$pistas[$p];

                        //======================================================
                        // arreglo que contiene toda la info 
                        // para llenar los controles de la vista

                        $info[$indice]['runway']=$pistas[$p];
                        $info[$indice]['star']=$sid_name;
                        //======================================================

                        if (count($transiciones)>0){
                            // con TRANSICIONES
                            for ($t=0;$t<count($transiciones);$t++){

                                $sid=$sid_name." / R.".$pistas[$p]." / T.".$transiciones[$t][0];  
                                
                                //======================================================
                                $info[$indice]['runway']=$pistas[$p];
                                $info[$indice]['star']=$sid_name;                                
                                $info[$indice]['transicion']=$transiciones[$t][0];
                                $indice++;
                                //======================================================

                                if (count($fijos[$pistas[$p]])>0){
                                    for ($f=0;$f<count($fijos[$pistas[$p]]);$f++){
                                        $sid=$sid." > ".$fijos[$pistas[$p]][$f];
                                    }// for
                                } // if


                                if (count($fijos_fin)>0){
                                    for ($ff=0;$ff<count($fijos_fin);$ff++){
                                        $sid=$sid." >> ".$fijos_fin[$ff];
                                    } // for
                                } // if
                                if (count($transiciones[$t])>0){    
                                    for ($f=1;$f<count($transiciones[$t]);$f++){
                                        $sid=$sid." | ".$transiciones[$t][$f];
                                    } // for
                                } //if                  
                                // echo "<BR> #### ".$sid."<BR>";
                            }
                        }else{
                             // sin TRANSICIONES
                              // var_dump($fijos);

                             //================================================
                             // Indice para ARREGLO DE INFO
                              $indice++;
                              //================================================

                              if (count($fijos)>0){
                                  for ($f=0;$f<count($fijos[$pistas[$p]]);$f++){
                                        $sid=$sid." -> ".$fijos[$pistas[$p]][$f];
                                  }
                              }
                            if (count($fijos_fin)>0){
                                for ($ff=0;$ff<count($fijos_fin);$ff++){
                                    $sid=$sid." => ".$fijos_fin[$ff];
                                }
                            }                          
                            // echo "<BR> **** ".$sid."<BR>";
                        }
                    } // for pistas

                    // echo "<br>#####################################################<br>";
                    // var_dump($aRNWS);
                    // var_dump($pistas);
                    // var_dump($fijos);
                    // echo "<br>#####################################################<br>";

                    $pistas=array();                
                    $transiciones=array();
                    $sid_name="";
                    $fijos=array();
                    $fijos_fin=array();
                    $cont_trans=-1;

                   

            }// si cont_sids>0


            // guardo nombre de SID
            $sid_name = $arre[1];  // nombre del SID

            // y si contiene RNW
            $pos=array_search("RNW",$arre);
            if ($pos !== false){
                $pistas[]=$arre[$pos+1];

                $pos=array_search("FIX",$arre);
                // si la linea tiene FIX
                if ($pos!==false){
                    // guardo todos los FIJOS INICIALES
                    for ($i=0;$i<count($arre);$i++){
                        if ($arre[$i]=="FIX"){
                            $index=1;
                            if ($arre[$i+1]=="OVERFLY"){
                                $index=2;
                            }
                            $fijos[$arre[$pos+1]][]=$arre[$i+$index];
                            // $fijos[$arre[$pos+1]][]=$afixes[$arre[$i+$index]];
                        } // if
                    } // for
                } // if pos!=false
            } // si NO tiene RNW
            else{
                // guardo todos los FIJOS FINALES
                for ($i=0;$i<count($arre);$i++){
                    if ($arre[$i]=="FIX"){
                        $index=1;
                        if ($arre[$i+1]=="OVERFLY"){
                            $index=2;
                        }
                        $fijos_fin[]=$arre[$i+$index];
                        // $fijos_fin[]=$afixes[$arre[$i+$index]];
                    } //if
                } // for                
            } // else

            $cont_sids++;
        } // si NO empieza con SID
        else{
            // si empieza con TRANSTIION
            if ($arre[0]=="TRANSITION"){

                $cont_trans++;
                $transit_name=$arre[1];

                // primer elemento es el nombre de TRANSICION
                $transiciones[$cont_trans][]=$transit_name;
                // guardo todos los FIJOS de la TRANSITION
                for ($i=0;$i<count($arre);$i++){
                    if ($arre[$i]=="FIX"){
                        // $transiciones[$cont_trans][]=$afixes[$arre[$i+1]];
                        $transiciones[$cont_trans][]=$arre[$i+1];
                    } // if                
                } // for
            } // si NO empieza con TRANSITION
            else{
                // si empieza con RNW
                if ($arre[0]=="RNW"){
                    $pistas[]=$arre[1];

                    $pos=array_search("FIX",$arre);
                    // si la linea tiene FIX
                    if ($pos!==false){
                        // guardo todos los FIJOS de la SID que empieza con RNW
                        for ($i=0;$i<count($arre);$i++){
                            if ($arre[$i]=="FIX"){
                               // $fijos[$arre[1]][]=$afixes[$arre[$i+1]];
                                $fijos[$arre[1]][]=$arre[$i+1];
                            } // if                
                        } // for
                    }// if pos!=false
                } // fin si NO empieza con TRANSITION y SI empieza con RNW
            }// fin si NO empieza con TRANSITION
        }
        // leer linea y explotarla
        $linea=trim(fgets($archi));
        $arre=explode(' ',$linea);

    }   // while

                   for ($p=0;$p<count($pistas);$p++){
                        $sid=$sid_name." / R.".$pistas[$p];

                        //======================================================
                        // arreglo que contiene toda la info 
                        // para llenar los controles de la vista
                        $info[$indice]['runway']=$pistas[$p];
                        $info[$indice]['star']=$sid_name;
                        //======================================================

                        if (count($transiciones)>0){
                            // con TRANSICIONES
                            for ($t=0;$t<count($transiciones);$t++){

                                $sid=$sid_name." / R.".$pistas[$p]." / T.".$transiciones[$t][0];                        

                                //======================================================
                                $info[$indice]['runway']=$pistas[$p];
                                $info[$indice]['star']=$sid_name;                                
                                $info[$indice]['transicion']=$transiciones[$t][0];
                                $indice++;
                                //======================================================


                                if (count($fijos[$pistas[$p]])>0){
                                    for ($f=0;$f<count($fijos[$pistas[$p]]);$f++){
                                        $sid=$sid." > ".$fijos[$pistas[$p]][$f];
                                    }
                                }


                                if (count($fijos_fin)>0){
                                    for ($ff=0;$ff<count($fijos_fin);$ff++){
                                        $sid=$sid." >> ".$fijos_fin[$ff];
                                    }
                                }
                                if (count($transiciones[$t])>0){    
                                    for ($f=1;$f<count($transiciones[$t]);$f++){
                                        $sid=$sid." | ".$transiciones[$t][$f];
                                    }
                                }                            
                                // echo "<BR> #### ".$sid."<BR>";
                            }
                        }else{
                             // sin TRANSICIONES
                             //================================================
                             // Indice para ARREGLO DE INFO
                              $indice++;
                              //================================================

                              for ($f=0;$f<count($fijos[$pistas[$p]]);$f++){
                                    $sid=$sid." -> ".$fijos[$pistas[$p]][$f];
                              }
                                if (count($fijos_fin)>0){
                                    for ($ff=0;$ff<count($fijos_fin);$ff++){
                                        $sid=$sid." => ".$fijos_fin[$ff];
                                    }
                                }                          
                                // echo "<BR> **** ".$sid."<BR>";
                        }
                    } // for pistas


    }   // function procesa_star



function get_procedimiento(){

        $resu="{ 'type': 'FeatureCollection',
            'features': [{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.410733333333, -31.489252783333]},
            'properties': {'fixname': '45DME'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.412913883333, -31.647052783333]},
            'properties': {'fixname': '50DME'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.367740916667, -31.335063666667]},
            'properties': {'fixname': 'AF001'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.320553083333, -31.346970966667]},
            'properties': {'fixname': 'AF002'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.242103383333, -31.387634233333]},
            'properties': {'fixname': 'AF003'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.215088466667, -31.41061215]},
            'properties': {'fixname': 'AF004'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.170223416667, -31.471194083333]},
            'properties': {'fixname': 'AF005'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.155489133333, -31.508452716667]},
            'properties': {'fixname': 'AF006'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.342671816667, -31.769896433333]},
            'properties': {'fixname': 'AF007'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.278955016667, -31.74340415]},
            'properties': {'fixname': 'AF008'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.226338733333, -31.702980866667]},
            'properties': {'fixname': 'AF009'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.17809075, -31.501185333333]},
            'properties': {'fixname': 'AF010'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.202582383333, -31.453705266667]},
            'properties': {'fixname': 'AF011'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.239573616667, -31.412541666667]},
            'properties': {'fixname': 'AF012'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.13571035, -31.507302816667]},
            'properties': {'fixname': 'AF013'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.151624833333, -31.4658922]},
            'properties': {'fixname': 'AF014'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.200375983333, -31.39965335]},
            'properties': {'fixname': 'AF015'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.2293205, -31.375037266667]},
            'properties': {'fixname': 'AF016'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.734555716667, -31.364423733333]},
            'properties': {'fixname': 'AF017'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.680808416667, -31.314588033333]},
            'properties': {'fixname': 'AF018'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.157402033333, -31.673179833333]},
            'properties': {'fixname': 'AF019'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.206446966667, -31.734178083333]},
            'properties': {'fixname': 'AF020'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.274379283333, -31.78028055]},
            'properties': {'fixname': 'AF021'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.388333333333, -31.331666666667]},
            'properties': {'fixname': 'AKROL'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.420077783333, -31.392525]},
            'properties': {'fixname': 'AKVUP'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.1025, -31.144444433333]},
            'properties': {'fixname': 'ASOTA'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.411944433333, -31.330277783333]},
            'properties': {'fixname': 'BITAG'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.400061116667, -31.414647216667]},
            'properties': {'fixname': 'CD18'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.404388883333, -31.730247216667]},
            'properties': {'fixname': 'CD36'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.419844433333, -31.41675]},
            'properties': {'fixname': 'CI18'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.414638883333, -31.229838883333]},
            'properties': {'fixname': 'D001T'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.385855566667, -31.315169433333]},
            'properties': {'fixname': 'D007O'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.374041666667, -31.232269433333]},
            'properties': {'fixname': 'D007T'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.345883333333, -31.321822216667]},
            'properties': {'fixname': 'D015O'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.367972216667, -31.422780566667]},
            'properties': {'fixname': 'D018I'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.314736116667, -31.424061116667]},
            'properties': {'fixname': 'D034J'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.282794433333, -31.382105566667]},
            'properties': {'fixname': 'D034M'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.272152783333, -31.368116666667]},
            'properties': {'fixname': 'D034N'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.258155566667, -31.356036116667]},
            'properties': {'fixname': 'D034O'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.190119433333, -31.439727783333]},
            'properties': {'fixname': 'D058N'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.173622216667, -31.430847216667]},
            'properties': {'fixname': 'D058O'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.168166666667, -31.552794433333]},
            'properties': {'fixname': 'D088M'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.148688883333, -31.551922216667]},
            'properties': {'fixname': 'D088N'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.129013883333, -31.554777783333]},
            'properties': {'fixname': 'D088O'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.170702783333, -31.596730566667]},
            'properties': {'fixname': 'D100M'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.151408333333, -31.599238883333]},
            'properties': {'fixname': 'D100N'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.133119433333, -31.607038883333]},
            'properties': {'fixname': 'D100O'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.190491666667, -31.6538]},
            'properties': {'fixname': 'D116M'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.172708333333, -31.6607]},
            'properties': {'fixname': 'D116N'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.360425, -31.808908333333]},
            'properties': {'fixname': 'D168O'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.370733333333, -31.861363883333]},
            'properties': {'fixname': 'D173R'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.395852783333, -31.813441666667]},
            'properties': {'fixname': 'D175O'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.597672216667, -31.492383333333]},
            'properties': {'fixname': 'D296J'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.77365, -31.420666666667]},
            'properties': {'fixname': 'D296T'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.808813883333, -31.406294433333]},
            'properties': {'fixname': 'D296V'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.609986116667, -31.271536116667]},
            'properties': {'fixname': 'D331T'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.412222216667, -31.781111116667]},
            'properties': {'fixname': 'DABUL'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.415663883333, -31.834008333333]},
            'properties': {'fixname': 'DILUX'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.296675, -31.244958333333]},
            'properties': {'fixname': 'DODBO'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-66.364722216667, -32.344166666667]},
            'properties': {'fixname': 'ESKOP'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.420908333333, -31.309011116667]},
            'properties': {'fixname': 'ESNUG'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.403616666667, -31.439516666667]},
            'properties': {'fixname': 'FD18'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.40695, -31.705288883333]},
            'properties': {'fixname': 'FD36'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.419608333333, -31.440133333333]},
            'properties': {'fixname': 'FI18'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.404802783333, -31.447805566667]},
            'properties': {'fixname': 'FS18'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.407802783333, -31.696969433333]},
            'properties': {'fixname': 'FS36'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.416502783333, -31.7505]},
            'properties': {'fixname': 'GEDOL'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-67.631388883333, -30.516388883333]},
            'properties': {'fixname': 'GILSA'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-67.841111116667, -31.639166666667]},
            'properties': {'fixname': 'ISUSA'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.421425, -31.563858333333]},
            'properties': {'fixname': 'JUA'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-67.835555566667, -31.5375]},
            'properties': {'fixname': 'KIBUN'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.119605566667, -31.595697216667]},
            'properties': {'fixname': 'KOTUG'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.418569433333, -31.543961116667]},
            'properties': {'fixname': 'MD18'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.418191666667, -31.595472216667]},
            'properties': {'fixname': 'MD36'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.336944433333, -32.058611116667]},
            'properties': {'fixname': 'MEBRA'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.316872216667, -31.39035]},
            'properties': {'fixname': 'NANEX'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.419247216667, -31.476038883333]},
            'properties': {'fixname': 'NIPTO'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.242741666667, -31.467661116667]},
            'properties': {'fixname': 'NU407'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.241641666667, -31.684980566667]},
            'properties': {'fixname': 'NU420'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.591197216667, -31.481894433333]},
            'properties': {'fixname': 'NU426'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.158255566667, -31.714286116667]},
            'properties': {'fixname': 'NU528'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.417766666667, -31.624311116667]},
            'properties': {'fixname': 'NU730'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-67.917805566667, -31.820477783333]},
            'properties': {'fixname': 'NU734'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.3975, -31.796944433333]},
            'properties': {'fixname': 'OPLET'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.308963883333, -31.749663883333]},
            'properties': {'fixname': 'POTSI'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.596813, -31.454895083333]},
            'properties': {'fixname': 'RF001'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.587174133333, -31.428865716667]},
            'properties': {'fixname': 'RF002'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.5647438, -31.409404]},
            'properties': {'fixname': 'RF003'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.24391625, -31.708280616667]},
            'properties': {'fixname': 'RF004'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.256976516667, -31.728768466667]},
            'properties': {'fixname': 'RF005'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.278416116667, -31.743283866667]},
            'properties': {'fixname': 'RF006'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.417338883333, -31.666988883333]},
            'properties': {'fixname': 'SEMLI'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.950277783333, -31.348333333333]},
            'properties': {'fixname': 'UKESO'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-68.534108333333, -31.400733333333]},
            'properties': {'fixname': 'UTLIX'}
            },{ 'type': 'Feature',
            'geometry': {'type': 'Point', 'coordinates': [-67.998625, -31.006738883333]},
            'properties': {'fixname': 'XONOM'}
            }]} ";

            return $resu;
}

}

