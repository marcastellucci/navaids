<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
	<link rel="stylesheet" type="text/css" href="<? echo base_url('bootstrap/css/bootstrap.css')?>" >
    <link rel="stylesheet" type="text/css" href="<? echo base_url('css/estilos.css');?>">
    
    <link rel="stylesheet" href="https://openlayers.org/en/v4.6.5/css/ol.css" type="text/css">    
    <script src="https://openlayers.org/en/v4.6.5/build/ol.js"></script>
	<title>SIDS y STARS - AIRACS de PMDG</title>

</head>

<body>	
	<header>
		<div class="container-fluid">
				    <h2><?php echo $application_name;?></h2>
		</div>
	</header>
  <div class="container-fluid">
      <section class="main row">
          <aside class="col-xs-12 col-sm-12 col-md-2">
            <form action="" class="action">
                <div class="form-group">
                  <div class="checkbox">
                      <input id="chk_animacion" name="chk_animacion" checked="" type="checkbox">
                      <label for="chk_animacion" class="label-success">Animación (ON/OFF)</label>
                  </div>  
                </div>
                <div class="form-group">
                  <label class="" id="lbl_sidstar" for="SID/STAR:">SID/STAR:</label>
                  <select class="form-control" id="selector_tipoprocedimiento" name="selector_tipoprocedimiento">
                    <option value="">Seleccione...</option>
                    <option value="SID">SID</option>
                    <option value="STAR">STAR</option>
                  </select>
                </div>  
                <div class="form-group">
                  <label class="" for="ICAO">ICAO:</label>
                  <input class="form-control" type="text" id="txt_icao" name="txt_icao" value="" placeholder="ICAO:">           
                </div>
                <div class="form-group">
                  <label class="" for="PISTA:">PISTA:</label>
                  <select class="form-control" id="selector_pistas" name="selector_pistas"></select>
                </div>
                <div class="form-group">
                  <label id="sid_star" class="" for="SID:">SID o STAR:</label>
                  <select class="form-control" id="selector_sids" name="selector_sids"></select>
                </div>
                <div class="form-group">
                  <label class="" for="TRANSICION:">TRANSICION:</label>
                  <select class="form-control" id="selector_transicion" name="selector_transicion"></select>
                </div>
              </form>

          </aside>        
          <article class="col-xs-12 col-sm-12 col-md-10">
              <div id="map" class="map">
                
              </div>
          </article>
      </section>
  </div>
  <footer>
      <div class="container-fluid">        
          <h6>(c) ASD - ArgentinaSceneryDesign 2018</h6>        
      </div>    
  </footer>

	<script src="<?= base_url('js/jquery-3.3.1.min.js');?>"></script>
	<script src="<?= base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('js/mapa.js'); ?>"></script>
	<script type="text/javascript">
// CODIGO DE LA VISTA ESPECIALMENTE
                  var base_url = '<?php echo base_url(); ?>';
                  var clics = 0;

                  var airport_data;

            $(document).ready(function () {               
               	
               	 //=================================================
               	 //   combo SELECTOR_SIDSTAR   (change)
               	 //=================================================
                 $('#selector_tipoprocedimiento').change(function(){   
                          $('#txt_icao').val('');       
                          $("#selector_pistas").empty();
                          $("#selector_sids").empty();
                          $('#selector_transicion').empty();       
                          
                          var sid_o_star = document.getElementById('selector_tipoprocedimiento').value;
                          $('#lbl_sidstar').text(sid_o_star);
                     }); // change               

               	 //=================================================
               	 //   input  TXT_ICAO   (change)
               	 //=================================================
                  // cuando cambia ICAO, buscar PISTAS, transiciones
                $('#txt_icao').change(function(){

                       if (document.getElementById('selector_tipoprocedimiento').value!=""){
                          var icao = document.getElementById('txt_icao').value;
                          icao = icao.toUpperCase();                                        
                          document.getElementById('txt_icao').value=icao;

                          // limpio los combos
                          $("#selector_pistas").empty();
                          $("#selector_sids").empty();
                          $('#selector_transicion').empty();

                          // limpio el mapa:


                            $.get( base_url+'index.php/consultas/buscar_pistas',
                            {   // parametros
                               _icao : icao
                            },
                            // resultado                            
                            // console.log(output[0]);

                            function(output){
  	                                
                                if (output != "vacio" && output != null) {
	                                  // Use jQuery's each to iterate over the opts value
	                                  // document.getElementById('geojsontext').value = output.centro[0].lon+' / '+output.
                                      $.each(output.pistas, function(id,value){
    	                                   $("#selector_pistas").append('<option value="'+value+'">'+value+'</option>');
                                      });

                                      // actualizo el mapa con los nuevos FEATURES:
                                      // if (document.getElementById('selector_tipoprocedimiento').value=="SID"){
                                      //       features_fijos.setStyle(SID_style);
                                      // }else{
                                      //     features_fijos.setStyle(STAR_style);
                                      // }
                                      
                                      // output.fixes TODOS
                                      UpdateMapGeoJSON(output, output.fixes[0]);


                                }else{
                                                      // $("#info_icao").html("some value");
                                }
                            },
                            "json");


                          }
                          else
                          {
                            alert('ERROR: Debe seleccionar un tipo de Procedimiento');
                            $('#selector_tipoprocedimiento').focus();
                          }
                  }); // change

               	 //=================================================
               	 //   combo SELECTOR_PISTAS   (change)
               	 //=================================================
                $('#selector_pistas').change(function(){

                            // alert(document.getElementById('selector_pistas').value);
                            var runway = document.getElementById('selector_pistas').value;
                            var sid_star = document.getElementById('selector_tipoprocedimiento').value;
                            var icao = document.getElementById('txt_icao').value;
                            var sidname = document.getElementById('selector_sids').value;
                            var transition = document.getElementById('selector_transicion').value; 
                            
                            $("#selector_sids").empty();
                            $('#selector_transicion').empty();                            

                            $.get( base_url+'index.php/consultas/buscar_sid_o_star',
                            {   // parametros
                               _icao : icao,
                               _sid_star: sid_star,
                               _pista : runway
                            },
                            function(output){                                
                                if (output != "vacio" && output != null) {
                                              // Use jQuery's each to iterate over the opts value
                                                      $.each(output, function(id,value){
                                                       $("#selector_sids").append('<option value="'+value+'">'+value+'</option>');
                                                           });                                  
                                }
                            },
                            "json");



                  }); // change


                  $('#selector_sids').change(function(){

                      if (document.getElementById('selector_pistas').value !== ''){

                          $('#selector_transicion').empty();

                          var procedimiento = document.getElementById('selector_tipoprocedimiento').value;
                          var icao = document.getElementById('txt_icao').value;
                          var runway = document.getElementById('selector_pistas').value;
                          var proc_name = document.getElementById('selector_sids').value;
                          var transition = document.getElementById('selector_transicion').value;
                          console.log(procedimiento+'-'+proc_name);

                          $('#selector_transicion').empty();
                          // $.get(
                          //      base_url+'index.php/consultas/get_transiciones',
                          //      {
                          //      _icao : icao,
                          //      _sid_o_star: procedimiento,
                          //      _pista : runway,
                          //      _sidstar_name: proc_name
                          //     },
                          //     function(output){
                          //       console.log(output);
                          //       if (output != "vacio" && output != null) {
                          //                     // Use jQuery's each to iterate over the opts value
                          //                             $.each(output, function(id,value){
                          //                              $("#selector_transicion").append('<option value="'+value+'">'+value+'</option>');
                          //                                  });                                  
                          //       }
                          //     },
                          //     "json");

//------------              
                            $.get( base_url+'index.php/consultas/get_airportdata',
                            {   // parametros
                               _icao : icao,
                               _runway: runway,
                               _proc_name : proc_name,
                               _transition: transition,
                               _procedimiento: procedimiento
                            },
                            function(output){
                                console.log(output);                                                
                                if (output != "vacio" && output != null) {
                                    //#########################################
                                          // Use jQuery's each to iterate over the opts value
                                          var transi=output['procedimiento'][proc_name]['only_transitions'];
                                          if (transi != null){
                                              $.each(transi, function(id,value){
                                                     $("#selector_transicion").append('<option value="'+value+'">'+value+'</option>');
                                                  });  
                                          }
                                    //#########################################
                                    ruta=output['procedimiento'][proc_name]['linestring'];
                                    UpdateMap_RUTA(ruta);

                                } //if

                            },
                            "json");

                      }
                      else{
                          alert('ERROR: Debe seleccionar un SID o STAR');
                      }

                  });


               $('#selector_transicion').change(function(){
                          var icao = document.getElementById('txt_icao').value;
                          var runway = document.getElementById('selector_pistas').value;
                          var procedimiento = document.getElementById('selector_tipoprocedimiento').value;
                          var proc_name = document.getElementById('selector_sids').value;                          
                          var transition = document.getElementById('selector_transicion').value;
                          
                            $.get( base_url+'index.php/consultas/get_airportdata',
                            {   // parametros
                               _icao : icao,
                               _runway: runway,
                               _procedimiento: procedimiento,
                               _proc_name : proc_name,
                               _transition: transition
                            },
                            function(output){
                                    
                                if (output != "vacio" && output != null) {

                                    ruta=output['procedimiento'][proc_name]['linestring'];
                                    UpdateMap_RUTA(ruta);

                                } //if

                            },
                            "json");
                          });   

              //=================== PREVENIR SUBMIT CON ENTER =====================
                   // $('form input').keydown(function(event){
                   //    if(event.keyCode == 13) {
                   //      event.preventDefault();
                   //      return false;
                   //    }
                   //  });
              //===============================================================

            });		
		
	</script>


</body>
</html>