<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
	<link rel="stylesheet" type="text/css" href="<? echo base_url('bootstrap/css/bootstrap.css')?>" >
    <link rel="stylesheet" type="text/css" href="<? echo base_url('css/estilos.css');?>">
    
    <link rel="stylesheet" href="https://openlayers.org/en/v4.6.5/css/ol.css" type="text/css">    
    <script src="https://openlayers.org/en/v4.6.5/build/ol.js"></script>
	<title>SIDS y STARS - PMDG AIRACS</title>

</head>

<body>	
	<header>
		<div class="container-fluid">
				    <h2><?php echo $application_name;?></h2>
		</div>
	</header>
  <div class="container-fluid">
      <section class="main row">
          <aside class="col-xs-12 col-sm-12 col-md-2">
            <form action="" class="action">
                <div class="form-group">
                  <div class="checkbox">
                      <input id="chk_animacion" name="chk_animacion" checked="" type="checkbox">
                      <label for="chk_animacion" class="label-success">Animación (ON/OFF)</label>
                  </div>  
                </div>
                <div class="form-group">
                  <label class="" id="lbl_sidstar" for="procedimiento:">PROCEDIMENTO:</label>
                  <select class="form-control" id="selector_tipoprocedimiento" name="selector_tipoprocedimiento">
                    <option value="">Seleccione...</option>
                    <option value="SID">SID</option>
                    <option value="STAR">STAR</option>
                  </select>
                </div>                  
                <div class="form-group">
                  <label class="" for="ICAO">ICAO:</label>
                  <input class="form-control" type="text" id="txt_icao" name="txt_icao" value="" placeholder="ICAO:">           
                </div>                
                <div class="form-group">
                  <label class="" for="PISTA:">PISTA:</label>
                  <select class="form-control" id="selector_pistas" name="selector_pistas"></select>
                </div>                

                <div class="form-group">
                  <label id="sid_star" class="" >SID o STAR:</label>
                  <select class="form-control" id="selector_sids" name="selector_sids"></select>
                </div>
                <div class="form-group">
                  <label class="" for="TRANSICION:">TRANSICION:</label>
                  <select class="form-control" id="selector_transicion" name="selector_transicion"></select>
                </div>
              </form>

          </aside>        
          <article class="col-xs-12 col-sm-12 col-md-10">
              <div id="map" class="map">
                
              </div>
          </article>
      </section>
  </div>
  <footer>
      <div class="container-fluid">        
          <h6>(c) ASD - ArgentinaSceneryDesign 2018</h6>        
      </div>    
  </footer>

	<script src="<?= base_url('js/jquery-3.3.1.min.js');?>"></script>
	<script src="<?= base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('js/mapa.js'); ?>"></script>
	<script type="text/javascript">
// CODIGO DE LA VISTA ESPECIALMENTE
                  var base_url = '<?php echo base_url(); ?>';
                  var clics = 0;

                  var airport_data;

            $(document).ready(function () {               
               	
               	 //=================================================
               	 //   combo SELECTOR_SIDSTAR   (change)
               	 //=================================================
                 $('#selector_tipoprocedimiento').change(function(){   
                          $('#txt_icao').val('');       
                          $("#selector_pistas").empty();
                          $("#selector_sids").empty();
                          $('#selector_transicion').empty();   

                          clear_route();    
                          
                          var sid_o_star = document.getElementById('selector_tipoprocedimiento').value;
                          $('#sid_star').text(sid_o_star);
                     }); // change               

               	 //=================================================
               	 //   input  ICAO   (change)
               	 //=================================================
                  // cuando cambia ICAO, buscar PISTAS, transiciones
                $('#txt_icao').change(function(){

                          var icao = document.getElementById('txt_icao').value;
                          icao = icao.toUpperCase();                                        
                          document.getElementById('txt_icao').value=icao;

                          // limpio los combos
                          $("#selector_pistas").empty();
                          $("#selector_sids").empty();
                          $('#selector_transicion').empty();

                          // limpio el mapa:
                            $.get( base_url+'index.php/controller_sidstars/get_airportdata',
                            {   // parametros
                               _icao : icao
                            },
                            // resultado                            
                            // console.log(output[0]);

                            function(output){
  	                               console.log(output);
                                if (output != null) {
	                                  // Use jQuery's each to iterate over the opts value
	                                  // document.getElementById('geojsontext').value = output.centro[0].lon+' / '+output.
                                      $.each(output['runways_list'], function(id,value){
    	                                   $("#selector_pistas").append('<option value="'+value+'">'+value+'</option>');
                                      });

                                      // output.fixes TODOS
                                      clear_route();
                                      UpdateMapGeoJSON(output, output.fixes);


                                }else{
                                                      // $("#info_icao").html("some value");
                                }
                            },
                            "json");

                  }); // change

               	 //=================================================
               	 //   combo SELECTOR_PISTAS   (change)
               	 //=================================================
                $('#selector_pistas').change(function(){

                    if ($('#selector_pistas').val()!=="seleccione..."){

                            // alert(document.getElementById('selector_pistas').value);
                            var runway = document.getElementById('selector_pistas').value;
                            var sid_star = document.getElementById('selector_tipoprocedimiento').value;
                            var icao = document.getElementById('txt_icao').value;
                            var sidname = document.getElementById('selector_sids').value;
                            var transition = document.getElementById('selector_transicion').value; 
                            
                            $("#selector_sids").empty();
                            $('#selector_transicion').empty();                            

                            $.get( base_url+'index.php/controller_sidstars/get_SIDoSTAR',
                            {   // parametros
                               _icao : icao,
                               _runway : runway,
                               _tipo_proc: sid_star
                            },
                            function(output){    
                                                         
                                if (output != null) {
                                    // Use jQuery's each to iterate over the opts value
                                    $.each(output, function(id,value){
                                     $("#selector_sids").append('<option value="'+value+'">'+value+'</option>');
                                         });                                  
                                }

                                clear_route();
                            },
                            "json");

                    }else{
                          $("#selector_sids").empty();
                    }

                  }); // change


                  ///================================================
                  //        NOMBRE DE SID O STAR
                  ///================================================
                  $('#selector_sids').change(function(){

                      if (document.getElementById('selector_pistas').value !== ''){

                          $('#selector_transicion').empty();

                          var procedimiento = document.getElementById('selector_tipoprocedimiento').value;
                          var icao = document.getElementById('txt_icao').value;
                          var runway = document.getElementById('selector_pistas').value;
                          var proc_name = document.getElementById('selector_sids').value;
                          var transition = document.getElementById('selector_transicion').value;
                          var tipo_proc = document.getElementById('selector_tipoprocedimiento').value;
                          
                          console.log(base_url+'index.php/controller_sidstars/get_TRANSITIONS?_icao='+icao);
                          console.log('&_proc_name='+proc_name+'&_procedimiento='+procedimiento+'&_runway='+runway);
                                                         

                          $('#selector_transicion').empty();
                          
                          $.get( base_url+'index.php/controller_sidstars/get_TRANSITIONS',
                            {   // parametros
                               _icao : icao,
                               _runway: runway,
                               _proc_name : proc_name,
                               _transition: transition,
                               _procedimiento: procedimiento
                            },
                            function(output){
                                console.log(output['route']);
                                if (output != null) {
                                    //#########################################
                                          // Use jQuery's each to iterate over the opts value
                                          $.each(output['transition_list'], function(id,value){
                                                    $("#selector_transicion").append('<option value="'+value+'">'+value+'</option>');
                                          });  
                                    //#########################################
                                    // ruta=output['procedimiento'][proc_name]['linestring'];
                                    UpdateMap_RUTA(output['route']);

                                } //if

                            },
                            "json");

                      }
                      else{
                          alert('ERROR: Debe seleccionar un SID o STAR');
                      }

                  });


               $('#selector_transicion').change(function(){
                          var icao = document.getElementById('txt_icao').value;
                          var runway = document.getElementById('selector_pistas').value;
                          var procedimiento = document.getElementById('selector_tipoprocedimiento').value;
                          var proc_name = document.getElementById('selector_sids').value;                          
                          var transition = document.getElementById('selector_transicion').value;

                          var ajax=base_url+'index.php/controller_sidstars/get_fullroute?';
                          ajax+='_icao='+icao;
                          ajax+='&_runway='+runway;
                          ajax+='&_proc_name='+proc_name;
                          ajax+='&_procedimiento='+procedimiento;
                          ajax+='&_transition='+transition;
                          console.log(ajax);
                          
                            $.get( base_url+'index.php/controller_sidstars/get_fullroute',
                            {   // parametros
                               _icao : icao,
                               _runway: runway,
                               _procedimiento: procedimiento,
                               _proc_name : proc_name,
                               _transition: transition
                            },
                            function(output){
                                console.log(output);
                                if (output != null) {

                                    // ruta=output['procedimiento'][proc_name]['linestring'];
                                    UpdateMap_RUTA(output['route']);

                                } //if

                            },
                            "json");
                          });   

              //=================== PREVENIR SUBMIT CON ENTER =====================
                   $('form input').keydown(function(event){
                      if(event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                      }
                    });
              //===============================================================

            });		
		
	</script>


</body>
</html>