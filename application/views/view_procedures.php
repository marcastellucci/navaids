<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0"> 
	<!-- <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no"> -->
	<link rel="stylesheet" type="text/css" href="<? echo base_url('bootstrap/css/bootstrap.css')?>" >
    <link rel="stylesheet" type="text/css" href="<? echo base_url('css/estilos.css');?>">
    
    <link rel="stylesheet" href="https://openlayers.org/en/v4.6.5/css/ol.css" type="text/css">    
    <script src="https://openlayers.org/en/v4.6.5/build/ol.js"></script>
	<title>SID/STAR - PMDG AIRACS</title>

</head>

<body>	
	<header>
		<div class="container-fluid">
      <div class="row">
        <div class="col-md-3">
            <h4><div id="airport" style="text-align: left;color: #FFD375;"></div></h4>
        </div>
        <div class="col-md-8">
				    <h2>DEPARTURE-ARRIVAL PROCEDURES</h2>
        </div>
        <div class="col-md-1"></div>
      </div>
		</div>
	</header>
  <div class="container-fluid">
      <section class="main row">
          <aside class="col-xs-12 col-sm-12 col-md-2">
            <form action="" class="action">
                <div class="form-group">
                  <div class="checkbox">
                      <input id="chk_animacion" name="chk_animacion" checked="" type="checkbox">
                      <label for="chk_animacion" class="label-success">Animation (ON/OFF)</label>
                  </div>  
                </div>
                <div class="form-group">
                  <label class="" id="lbl_sidstar" for="procedimiento:">PROCEDURE:</label>
                  <select class="form-control" id="selector_tipoprocedimiento" name="selector_tipoprocedimiento">
                    <option value="">seleccione...</option>
                    <option value="SID">SID</option>
                    <option value="STAR">STAR</option>
                  </select>
                </div>                  
                <div class="form-group">
                  <label class="" for="ICAO">ICAO:</label>
                  <input class="form-control" type="text" id="txt_icao" name="txt_icao" value="" placeholder="ICAO:">           
                </div>                
                <div class="form-group">
                  <label class="" for="PISTA:">RUNWAY:</label>
                  <select class="form-control" id="selector_pistas" name="selector_pistas"></select>
                </div>                

                <div class="form-group">
                  <label id="sid_star" class="">SID / STAR:</label>
                  <div class="checkbox_app">
                        <input id="chk_final_approach" name="chk_final_approach" checked="" type="checkbox">
                        <label for="chk_final_approach" class="label-success">Incluir tramo a pista</label>
                  </div> 
                  <select class="form-control" id="selector_sids" name="selector_sids"></select>
                </div>
                <div class="form-group">
                  <label class="" for="TRANSICION:">TRANSITION:</label>
                  <select class="form-control" id="selector_transicion" name="selector_transicion"></select>
                </div>
                <div class="div_approaches">
                  <div class="form-group">
                    <label class="" for="APPROACHES:">APPROACHES:</label>
                    <select class="form-control" id="selector_approaches" name="selector_approaches"></select>
                  </div>
                  <div class="form-group">
                    <label class="" for="APPROACHES:">APPROACHE TRANSITIONS:</label>
                    <select class="form-control" id="selector_app_transitions" name="selector_app_transitions"></select>
                  </div>                
                </div>
                <div class="form-group">
                  <label class="" >Find FIX:</label>
                  <input class="form-control" type="text" id="txt_fix" name="txt_fix" value="" placeholder="">
                </div>  
                
                  <button type="button" id="btn_vectores" class="btn btn-primary">VECTORES</button>
                
               </form>

          </aside>        
          <article class="col-xs-12 col-sm-12 col-md-10">
              <div id="map" class="map">               
              </div>
          </article>
      </section>
  </div>
  <footer>
      <div class="container-fluid">        
          <h6>(c) ASD - ArgentinaSceneryDesign 2018  -  versión 2.0</h6>        
      </div>    
  </footer>
  
	<script src="<?= base_url('js/jquery-3.3.1.min.js');?>"></script>
	<script src="<?= base_url('bootstrap/js/bootstrap.min.js'); ?>"></script>
	<script src="<?= base_url('js/mapa.js'); ?>"></script>
	<script type="text/javascript">
// CODIGO DE LA VISTA ESPECIALMENTE
              var base_url = '<?php echo base_url(); ?>';
              var clics = 0;

              var airport_data;

            $(document).ready(function () {               
               	
               	 //=================================================
               	 //   combo SELECTOR_SIDSTAR   (change)
               	 //=================================================
                 $('#selector_tipoprocedimiento').change(function(){   
                          $('#txt_icao').val('');       
                          $("#selector_pistas").empty();
                          $("#selector_sids").empty();
                          $('#selector_transicion').empty();
                          $('#selector_approaches').empty();   

                          clear_route();
                          clear_approach();
                          
                          var sid_o_star = document.getElementById('selector_tipoprocedimiento').value;
                          $('#sid_star').text(sid_o_star);

                          // if ($('#selector_tipoprocedimiento').val()=="SID"){
                          //     $('#selector_approaches').attr('disabled', 'disabled');
                          //     $('#selector_app_transitions').attr('disabled', 'disabled');                          
                          // }else{
                          //     $('#selector_approaches').removeAttr('disabled');
                          //     $('#selector_app_transitions').removeAttr('disabled');
                          // }

                          if ($('#selector_tipoprocedimiento').val()=="SID"){
                            $('.div_approaches').hide();
                            $('.checkbox_app').hide();
                          }else{
                            $('.div_approaches').show();
                            $('.checkbox_app').show();
                          }


                     }); // change               

               	 //=================================================
               	 //   input  ICAO   (change)
               	 //=================================================
                  // cuando cambia ICAO, buscar PISTAS, transiciones
                $('#txt_icao').change(function(){

                          var icao = document.getElementById('txt_icao').value;
                          icao = icao.toUpperCase();                                        
                          document.getElementById('txt_icao').value=icao;

                          // limpio los combos
                          $("#selector_pistas").empty();
                          $("#selector_sids").empty();
                          $('#selector_transicion').empty();
                          $('#selector_approaches').empty();

                          // console.log(base_url+'index.php/controller_procedures/get_airportdata?icao='+icao);

                          // limpio el mapa:
                            $.get( base_url+'index.php/controller_procedures/get_airportdata',
                            {   // parametros
                               icao : icao
                            },
                            // resultado                            
                            // console.log(output[0]);

                            function(output){
                              
                                if (output != null) {
	                                  // Use jQuery's each to iterate over the opts value
	                                  // document.getElementById('geojsontext').value = output.centro[0].lon+' / '+output.
                                      $.each(output['runway_list'], function(id,value){
    	                                   $("#selector_pistas").append('<option value="'+id+'">'+value+'</option>');
                                      });

                                      $.each(output['approaches_list'], function(id,value){
                                         $("#selector_approaches").append('<option value="'+id+'">'+value+'</option>');
                                      });                                      

                                      if (output['airport_name']!=""){
                                         document.getElementById('airport').innerHTML = $('#txt_icao').val()+' - '+output['airport_name'];
                                      }

                                      // output.fixes TODOS
                                      clear_route();
                                      clear_approach();
                                      UpdateMapGeoJSON(output, output.geojson_fixs);


                                }else{
                                                      // $("#info_icao").html("some value");
                                }
                            },
                            "json");

                  }); // change

               	 //=================================================
               	 //   combo SELECTOR_PISTAS   (change)
               	 //=================================================
                $('#selector_pistas').change(function(){

                    if ($('#selector_pistas').val()!=="0"){

                            // alert(document.getElementById('selector_pistas').value);
                            var runway = document.getElementById('selector_pistas').value;
                            var sid_star = document.getElementById('selector_tipoprocedimiento').value;
                            var icao = document.getElementById('txt_icao').value;
                            var sidname = document.getElementById('selector_sids').value;
                            var transition = document.getElementById('selector_transicion').value; 
                            
                            $("#selector_sids").empty();
                            $('#selector_transicion').empty();                            

                            $.get( base_url+'index.php/controller_procedures/get_procedures',
                            {   // parametros
                               icao : icao,
                               runway : runway,
                               tipo_proc: sid_star
                            },
                            function(output){    
     
                                if (output != null) {
                                    // Use jQuery's each to iterate over the opts value
                                    $.each(output.procedure_list, function(id,value){
                                     $("#selector_sids").append('<option value="'+id+'">'+value+'</option>');
                                         });                                  
                                }

                                clear_route();
                            },
                            "json");

                    }else{
                          $("#selector_sids").empty();
                    }

                  }); // change


                  ///================================================
                  //        NOMBRE DE SID O STAR
                  ///================================================
                  $('#selector_sids').change(function(){

                       // limpio las rutas que existan
                        clear_route();

                      var runway = document.getElementById('selector_pistas').value;
                      var procedimiento = document.getElementById('selector_tipoprocedimiento').value;

                      if (procedimiento!='0' && runway!='0'){

                          $('#selector_transicion').empty();

                          var icao = document.getElementById('txt_icao').value;
                          var proc_name = document.getElementById('selector_sids').value;
                          var transition = document.getElementById('selector_transicion').value;
                          var tipo_proc = document.getElementById('selector_tipoprocedimiento').value;

                          // var cad=base_url+'index.php/controller_procedures/get_transitions';
                          // cad+="?icao="+icao+"&runway="+runway;
                          // cad+="&proc_name="+proc_name;
                          // cad+="&tipo_proc="+tipo_proc;
                          // console.log(cad);


                          $('#selector_transicion').empty();
                          
//                          $.get( base_url+'index.php/controller_sidstars/get_TRANSITIONS',
                          $.get( base_url+'index.php/controller_procedures/get_transitions',
                            {   // parametros
                               icao : icao,
                               runway: runway,
                               proc_name : proc_name,
                               tipo_proc: tipo_proc
                               // _transition: transition,
                               
                            },
                            function(output){

                                if (output != null) {
                                    //#########################################
                                          // Use jQuery's each to iterate over the opts value
                                          $.each(output.transition_list, function(id,value){
                                                    $("#selector_transicion").append('<option value="'+id+'">'+value+'</option>');
                                          });  
                                    //#########################################
                                    // ruta=output['procedimiento'][proc_name]['linestring'];
                                    UpdateMap_RUTA(output['route']);

                                    // si el checkbox (TRAMO FINAL = true)
                                    // muestro tramo final del STAR

                                    var con_final_app=document.getElementById('chk_final_approach').checked;  
                                    // if (tipo_proc=="STAR" && ('#chk_final_approach').prop('checked',true) ){
                                    if (tipo_proc=="STAR" && con_final_app ){
                                        Update_RUTA_FINAL_APP(output['route_final']);
                                    }

                                } //if

                            },
                            "json");

                      }
                      else{
                         //  alert('ERROR: Debe seleccionar un SID o STAR');
                      }

                  });

                  ///================================================
                  //        SELECTOR DE TRANSICIONES
                  ///================================================
               $('#selector_transicion').change(function(){
                          var icao = document.getElementById('txt_icao').value;
                          var runway = document.getElementById('selector_pistas').value;
                          var procedimiento = document.getElementById('selector_tipoprocedimiento').value;
                          var proc_name = document.getElementById('selector_sids').value;                          
                          var transition = document.getElementById('selector_transicion').value;

                          // var ajax=base_url+'index.php/controller_procedures/get_fullroute?';
                          // ajax+='icao='+icao;
                          // ajax+='&runway='+runway;
                          // ajax+='&proc_name='+proc_name;
                          // ajax+='&tipo_proc='+procedimiento;
                          // ajax+='&transition='+transition;
                          // console.log(ajax);
                          
                            $.get( base_url+'index.php/controller_procedures/get_fullroute',
                            {   // parametros
                               icao : icao,
                               runway: runway,
                               tipo_proc: procedimiento,
                               proc_name : proc_name,
                               transition: transition
                            },
                            function(output){
                                if (output != null) {

                                    // ruta=output['procedimiento'][proc_name]['linestring'];
                                    UpdateMap_RUTA(output['route']);

                                } //if

                            },
                            "json");
                          });

                  ///================================================
                  //        SELECTOR DE APPROACHES
                  ///================================================
               $('#selector_approaches').change(function(){

                    if ($('#selector_approaches').val()!=""){

                          var icao = document.getElementById('txt_icao').value;
                          var runway = document.getElementById('selector_pistas').value;
                          var approach = document.getElementById('selector_approaches').value;

                          var ajax=base_url+'index.php/controller_procedures/get_approach?';
                          ajax+='icao='+icao;
                          ajax+='&approach='+approach;
                          console.log(ajax);

                          $('#selector_app_transitions').empty();
                          
                            $.get( base_url+'index.php/controller_procedures/get_approach',
                            {   // parametros
                               icao : icao,
                               approach: approach
                            },
                            function(output){
                                // console.log(output);
                                if (output != null) {
                                     $.each(output.transitions_list, function(id,value){
                                     $("#selector_app_transitions").append('<option value="'+id+'">'+value+'</option>');
                                         });   

                                    Update_RUTA_APPROACH(output['route']);
                                   
                                } //if
                            },
                  
                              "json");
                    }else{
                      clear_approach();
                      $('#selector_app_transitions').empty();
                    }
                  });             

                  ///================================================
                  //        SELECTOR DE APPROACHES TRANSITIONS
                  ///================================================
               $('#selector_app_transitions').change(function(){

                          var icao = document.getElementById('txt_icao').value;
                          var approach = document.getElementById('selector_approaches').value;
                          var transition_app = document.getElementById('selector_app_transitions').value;

                          // var ajax=base_url+'index.php/controller_procedures/get_approach_transitions?';
                          // ajax+='icao='+icao;
                          // ajax+='&approach='+approach;
                          // ajax+='&transition_app='+transition_app;

                          // console.log(ajax);
                          
                            $.get( base_url+'index.php/controller_procedures/get_approach_transitions',
                            {   // parametros
                               icao : icao,
                               approach: approach,
                               transition_app: transition_app
                            },
                            function(output){                                
                                if (output != null) {
                                    Update_RUTA_APPROACH(output['route']);                                   
                                } //if
                            },
                            "json");
                          });                                                 

                  //=================================================
                 //   input  FIX   (change)
                 //=================================================
                  // cuando cambia FIX, buscar UN FIX
                $('#txt_fix').change(function(){

                          var fix = document.getElementById('txt_fix').value;
                          fix = fix.toUpperCase(); 

                          buscar_fix(fix);

                  }); // change

              //=================== PREVENIR SUBMIT CON ENTER =====================
                   $('form input').keydown(function(event){
                      if(event.keyCode == 13) {
                        event.preventDefault();
                        return false;
                      }
                    });
              //===============================================================

              $('#btn_vectores').click(function(){
                    var icao = document.getElementById('txt_icao').value;
                    $.get( base_url+'index.php/controller_procedures/vectores',
                            {   // parametros
                               icao : icao
                            },
                            function(output){                                
                                console.log(output['vector']);
                                
                                if (output != null) {
                                    UpdateMap_RUTA(output['vector']);
                                } //if
                            },
                            "json");
              }); // change



            })
		
	</script>


</body>
</html>